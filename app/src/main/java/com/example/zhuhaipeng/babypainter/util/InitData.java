package com.example.zhuhaipeng.babypainter.util;

import java.util.HashMap;
import java.util.Map;

/**
 * @author gg_boy
 * @version $Rev$
 * @des ${初始化数据}
 * @updateAuther $Auther$
 * @updateDes ${TODO}
 */
public class InitData {
    private Map<String,String> songlist;
    private Map<String,String> teachList;
    private Map<String,String> songVideoList;


    public Map<String, String> getSonglist() {
        initSongDate();
        return songlist;
    }

    public Map<String, String> getTeachList() {
        initTeachDate();
        return teachList;
    }

    public Map<String, String> getSongVideoList() {
        initSongVideoList();
        return songVideoList;
    }
    private static class Holder{
        private static final InitData INIT_DATA = new InitData();
    }
    public static InitData getInstance(){
        return Holder.INIT_DATA;
    }

    /**
     * 初始化歌曲数据
     * @return
     */
    public Map<String,String> initSongDate(){
        songlist = new HashMap<>();
        songlist.put("turtle","http://bmob-cdn-19476.b0.upaiyun.com/2018/11/08/b986917040c6e5f7807c31a352be6565.mp3");
        songlist.put("rabbit","http://bmob-cdn-19476.b0.upaiyun.com/2018/11/08/4150ac6a40de2c89807e5d15f3565f9e.mp3");
        songlist.put("dog","http://bmob-cdn-19476.b0.upaiyun.com/2018/11/08/77fc123240630a5480cd0f96618da127.mp3");
        return songlist;
    }

    /**
     *  初始化歌曲视频数据
     * @return
     */
    public  Map<String,String> initSongVideoList(){
        songVideoList = new HashMap<>();
        songVideoList.put("apple","http://bmob-cdn-19476.b0.upaiyun.com/2018/11/09/cbd51dae40d4d7f480888608b9980031.mp4");
        return songVideoList;
    }

    /**
     *  初始化教学视频数据
     * @return
     */
    public Map<String,String> initTeachDate() {
        teachList = new HashMap<>();
        teachList.put("apple","http://bmob-cdn-19476.b0.upaiyun.com/2018/11/08/91cd34884002299c80eccbcd45782c5e.mp4");
        return teachList;
    }
}
