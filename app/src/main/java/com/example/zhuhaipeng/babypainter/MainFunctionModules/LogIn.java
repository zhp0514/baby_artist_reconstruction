package com.example.zhuhaipeng.babypainter.MainFunctionModules;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.zhuhaipeng.babypainter.AllBmobClass.MyUser;
import com.example.zhuhaipeng.babypainter.R;

import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.SaveListener;

public class LogIn extends AppCompatActivity {
    private EditText user;
    private EditText password;
    private static long exitTime = 0;
    private static final String TAG = "LogIn";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);
        password = (EditText) findViewById(R.id.et_login_password);
        user = (EditText) findViewById(R.id.et_log_user_name);
        Button button = (Button) findViewById(R.id.btn_login_register);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logIn(v);
            }
        });
    }
    // 填写用户名和密码
    private void logIn(final View view){
        MyUser bu2 = new MyUser();
        bu2.setUsername(user.getText().toString());
        bu2.setPassword(password.getText().toString());
        Log.e(TAG, "user:"+user.getText().toString()+",password:"+password.getText().toString());
        bu2.login(new SaveListener<MyUser>() {
            @Override
            public void done(MyUser bmobUser, BmobException e) {
                if(e == null){
                    Toast.makeText(LogIn.this,"登录成功:",Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(LogIn.this,MainActivityShow.class); // 前往主页
                    startActivity(intent);
                    finish();
                    //通过BmobUser user = BmobUser.getCurrentUser()获取登录成功后的本地用户信息
                    //如果是自定义用户对象MyUser，可通过MyUser user = BmobUser.getCurrentUser(MyUser.class)获取自定义用户信息
                }else{
                    Log.e(TAG, "Message "+e.getMessage() );
                    // 出现这个错误java.net.UnknownServiceException: CLEARTEXT communication ** not permitted by
                    // 的原因是 Android p版本过高，不支持非加密的明文流量的http网络请求
                    Toast.makeText(LogIn.this,"登录失败，请检查账号和密码是否匹配",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode==KeyEvent.KEYCODE_BACK){
            if (System.currentTimeMillis() - exitTime > 2000) {
                Toast.makeText(LogIn.this, "再按一次退出程序", Toast.LENGTH_SHORT).show();
                exitTime = System.currentTimeMillis();
            }else{
                moveTaskToBack(true);
            }
        }
        return super.onKeyDown(keyCode, event);
    }
}
