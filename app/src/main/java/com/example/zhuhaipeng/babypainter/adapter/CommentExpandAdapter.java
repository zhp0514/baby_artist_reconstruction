package com.example.zhuhaipeng.babypainter.adapter;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.zhuhaipeng.babypainter.AllBmobClass.Replay;
import com.example.zhuhaipeng.babypainter.R;
import com.example.zhuhaipeng.babypainter.bean.Comment;
import com.example.zhuhaipeng.babypainter.bean.ReplyDetailBean;


import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Author: Moos
 * E-mail: moosphon@gmail.com
 * Date:  18/4/20.
 * Desc: 评论与回复列表的适配器
 */

public class CommentExpandAdapter extends BaseExpandableListAdapter {
    private static final String TAG = "CommentExpandAdapter";
    private List<Comment> commentBeanList;
    private List<Replay> replyList;
    private Context context;
    private int pageIndex = 1;

    public CommentExpandAdapter(Context context, List<Comment> commentBeanList) {
        this.context = context;
        this.commentBeanList = commentBeanList;
    }
    private void query(int pos){

    }
    // 返回group分组的数量，在当前需求中指代评论的数量。
    @Override
    public int getGroupCount() {
        return commentBeanList.size();
    }
    // 返回所在group中child的数量，这里指代当前评论对应的回复数目。
    @Override
    public int getChildrenCount(int i) {
        Log.e(TAG, "回复数量 "+commentBeanList.get(i).getReplyTotal() ); // 这里为空，，回复存在bug 日期：2018/11/22 23：07
        if(commentBeanList.get(i).getReplyList() == null){
            return 0;
        }else {
            return commentBeanList.get(i).getReplyList().size() >0 ? commentBeanList.get(i).getReplyList().size():0;
        }

    }

    //返回group的实际数据，这里指的是当前评论数据。
    @Override
    public Object getGroup(int i) {
        return commentBeanList.get(i);
    }

    //返回group中某个child的实际数据，这里指的是当前评论的某个回复数据
    @Override
    public Object getChild(int i, int i1) {
        return commentBeanList.get(i).getReplyList().get(i1);
    }

    //返回分组的id，一般将当前group的位置传给它。
    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    // 返回分组中某个child的id，一般也将child当前位置传给它，不过为了避免重复，
    // 可以使用getCombinedChildId(groupPosition, childPosition);来获取id并返回
    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return getCombinedChildId(groupPosition, childPosition);
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }
    boolean isLike = false;

    //
    @Override
    public View getGroupView(final int groupPosition, boolean isExpand, View convertView, ViewGroup viewGroup) {
        final GroupHolder groupHolder;
        if(convertView == null){
            convertView = LayoutInflater.from(context).inflate(R.layout.comment_item_layout, viewGroup, false);
            groupHolder = new GroupHolder(convertView);
            convertView.setTag(groupHolder);
        }else {
            groupHolder = (GroupHolder) convertView.getTag();
        }

        Comment comment = this.commentBeanList.get(groupPosition);
        Glide.with(context).load(comment.getUser().getImage()).into(groupHolder.logo);
        groupHolder.tv_name.setText(commentBeanList.get(groupPosition).getUser().getUsername());
        groupHolder.tv_time.setText(commentBeanList.get(groupPosition).getCreateDate());
        groupHolder.tv_content.setText(commentBeanList.get(groupPosition).getContent());
        groupHolder.iv_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isLike){
                    isLike = false;
                    groupHolder.iv_like.setColorFilter(Color.parseColor("#aaaaaa"));
                }else {
                    isLike = true;
                    groupHolder.iv_like.setColorFilter(Color.parseColor("#FF5C5C"));
                }
            }
        });

        return convertView;
    }

    @Override
    public View getChildView(final int groupPosition, int childPosition, boolean b, View convertView, ViewGroup viewGroup) {
        final ChildHolder childHolder;
        if(convertView == null){
            convertView = LayoutInflater.from(context).inflate(R.layout.comment_reply_item_layout,viewGroup, false);
            childHolder = new ChildHolder(convertView);
            convertView.setTag(childHolder);
        }
        else {
            childHolder = (ChildHolder) convertView.getTag();
        }

        if(commentBeanList.get(groupPosition).getReplyList()!=null){
            String replyUser = commentBeanList.get(groupPosition).getReplyList().get(childPosition).getNickName();
            if(!TextUtils.isEmpty(replyUser)){
                childHolder.tv_name.setText(replyUser + ":");
            }else {
                childHolder.tv_name.setText("无名"+":");
            }

            childHolder.tv_content.setText(commentBeanList.get(groupPosition).getReplyList().get(childPosition).getContent());
        }

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return true;
    }

    private class GroupHolder{
        private CircleImageView logo;
        private TextView tv_name, tv_content, tv_time;
        private ImageView iv_like;
        public GroupHolder(View view) {
            logo = (CircleImageView) view.findViewById(R.id.comment_item_logo);
            tv_content = (TextView) view.findViewById(R.id.comment_item_content);
            tv_name = (TextView) view.findViewById(R.id.comment_item_userName);
            tv_time = (TextView) view.findViewById(R.id.comment_item_time);
            iv_like = (ImageView) view.findViewById(R.id.comment_item_like);
        }
    }

    private class ChildHolder{
        private TextView tv_name, tv_content;
        public ChildHolder(View view) {
            tv_name = (TextView) view.findViewById(R.id.reply_item_user);
            tv_content = (TextView) view.findViewById(R.id.reply_item_content);
        }
    }


    /**
     * by moos on 2018/04/20
     * func:评论成功后插入一条数据
     * @param comment 新的评论数据
     */
    public void addTheCommentData(Comment comment){
        if(comment !=null){
            commentBeanList.add(comment);
            notifyDataSetChanged();
        }else {
            throw new IllegalArgumentException("评论数据为空!");
        }

    }

    /**
     * by moos on 2018/04/20
     * func:回复成功后插入一条数据
     * @param replyComment 新的回复数据
     */
    public void addTheReplyData(ReplyDetailBean replyComment, int groupPosition){
        if(replyComment!=null){
            Log.e(TAG, "addTheReplyData: >>>>该刷新回复列表了:"+replyComment.getContent()+"位置"+groupPosition);
            if(commentBeanList.get(groupPosition).getReplyList() != null ){
                commentBeanList.get(groupPosition).getReplyList().add(replyComment);
            }else {
                List<ReplyDetailBean> replyList = new ArrayList<>();
                replyList.add(replyComment);
                commentBeanList.get(groupPosition).setReplyList(replyList);
            }
            notifyDataSetChanged();
        }else {
            throw new IllegalArgumentException("回复数据为空!");
        }

    }

    /**
     * by moos on 2018/04/20
     * func:添加和展示所有回复
     * @param replyComment 所有回复数据
     * @param groupPosition 当前的评论
     */
    private void addReplyList(List<ReplyDetailBean> replyComment, int groupPosition){
        if(commentBeanList.get(groupPosition).getReplyList() != null ){
            commentBeanList.get(groupPosition).getReplyList().clear();
            commentBeanList.get(groupPosition).getReplyList().addAll(replyComment);
        }else {

            commentBeanList.get(groupPosition).setReplyList(replyComment);
        }

        notifyDataSetChanged();
    }

}
