package com.example.zhuhaipeng.babypainter.bean;

import com.example.zhuhaipeng.babypainter.AllBmobClass.MyUser;
import com.example.zhuhaipeng.babypainter.AllBmobClass.Post;
import com.example.zhuhaipeng.babypainter.AllBmobClass.Replay;

import java.util.List;

import cn.bmob.v3.BmobObject;

/**
 * Created by moos on 2018/4/20.
 */

public class Comment extends BmobObject {
    private int id;
    private String content;
    private String imgId;
    private Integer replyTotal;
    private String createDate;

    private List<ReplyDetailBean> reply;

    private Replay replay; //对应评论的回复

    private MyUser user;//评论的用户，Pointer类型，一对一关系

    private Post post; //所评论的帖子，这里体现的是一对多的关系，一个评论只能属于一个微博

    public Comment(String content, String createDate) {
        this.content = content;
        this.createDate = createDate;
    }
    public Comment(){ }
    public Replay getReplay() {
        return replay;
    }

    public void setReplay(Replay replay) {
        this.replay = replay;
    }
    public MyUser getUser() {
        return user;
    }

    public void setUser(MyUser user) {
        this.user = user;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }
    public void setId(int id) {
        this.id = id;
    }
    public int getId() {
        return id;
    }

    public void setContent(String content) {
        this.content = content;
    }
    public String getContent() {
        return content;
    }

    public void setImgId(String imgId) {
        this.imgId = imgId;
    }
    public String getImgId() {
        return imgId;
    }

    public void setReplyTotal(Integer replyTotal) {
        this.replyTotal = replyTotal;
    }
    public Integer getReplyTotal() {
        return replyTotal;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }
    public String getCreateDate() {
        return createDate;
    }

    public void setReplyList(List<ReplyDetailBean> replyList) {
        this.reply = replyList;
    }
    public List<ReplyDetailBean> getReplyList() {
        return reply;
    }
}
