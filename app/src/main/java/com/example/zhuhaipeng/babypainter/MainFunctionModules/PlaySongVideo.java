package com.example.zhuhaipeng.babypainter.MainFunctionModules;

import android.media.Image;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import com.example.zhuhaipeng.babypainter.R;
import com.example.zhuhaipeng.babypainter.util.InitData;
import com.example.zhuhaipeng.babypainter.util.Util;
import com.example.zhuhaipeng.babypainter.util.UtilForPicture;

/**
 * @author gg_boy
 * @version $Rev$
 * @des ${播放儿歌}
 * @updateAuther $Auther$
 * @updateDes ${TODO}
 */
public class PlaySongVideo extends AppCompatActivity implements View.OnClickListener{
    private InitData mInitData = null;
    private  static  MediaPlayer mediaPlayer  = new MediaPlayer();
    private boolean isPlaying = false; // 是否正在播放

    private LinearLayout back;
    private VideoView show;

    private static final String TAG = "PlaySongVideo";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_song_video);
        mInitData = InitData.getInstance();
        initCotrol();
        startPlay();
    }

    public void startPlay(){
        // 如果没有MV
        if(mInitData.getSongVideoList().get(UtilForPicture.getInfoFromServer()) == null
        && mInitData.getSonglist().get(UtilForPicture.getInfoFromServer()) != null){
            Toast.makeText(PlaySongVideo.this,"暂未添加该种类儿歌MV视频,只有儿歌歌曲播放",Toast.LENGTH_LONG).show();
            show.setVideoPath(mInitData.getSonglist().get(UtilForPicture.getInfoFromServer()));
//            try{
//                mediaPlayer.setDataSource(mInitData.getSonglist().get(UtilForPicture.getInfoFromServer()));
//                //准备播放
//                mediaPlayer.prepareAsync();
//                //3.1 设置一个准备完成的监听
//                mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
//                    @Override
//                    public void onPrepared(MediaPlayer mp) {
//                        // 4 开始播放
//                        mediaPlayer.start();
//                        isPlaying = true;
//                    }
//                });
//            }catch (Exception e){
//                e.printStackTrace();
//            }
        }else if(mInitData.getSongVideoList().get(UtilForPicture.getInfoFromServer()) != null){ // 有MV
            show.setVideoPath(mInitData.getSongVideoList().get(UtilForPicture.getInfoFromServer()));
//            show.start();
        }
        //创建MediaController对象
        MediaController mediaController = new MediaController(this);

        //VideoView与MediaController建立关联
        show.setMediaController(mediaController);
        mediaController.show();
        //让VideoView获取焦点
        show.requestFocus();

    }

    public void initCotrol(){
        back = findViewById(R.id.iv_go_back_in_song_video);
        show = findViewById(R.id.vv_show_video);

        back.setOnClickListener(this);
        show.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.iv_go_back_in_song_video:
//                mediaPlayer.stop();
//                mediaPlayer.release();
                Log.e(TAG, "PlaySongVideo销毁");
                finish();
                break;
            default:
                break;
        }
    }
}
