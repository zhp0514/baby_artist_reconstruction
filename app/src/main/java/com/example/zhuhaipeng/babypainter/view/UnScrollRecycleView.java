package com.example.zhuhaipeng.babypainter.view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

public class UnScrollRecycleView extends RecyclerView
{
    public UnScrollRecycleView(Context context){
        super(context);
    }
    public UnScrollRecycleView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
    public UnScrollRecycleView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2,
                MeasureSpec.AT_MOST);
        super.onMeasure(widthMeasureSpec, expandSpec);//这里返回的是刚写好的expandSpec
    }
}
