package com.example.zhuhaipeng.babypainter.UserInfo;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.ContentUris;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.zhuhaipeng.babypainter.AllBmobClass.MyUser;
import com.example.zhuhaipeng.babypainter.R;
import com.example.zhuhaipeng.babypainter.util.UtilForPicture;


import java.io.File;
import java.io.FileNotFoundException;

import cn.bmob.v3.BmobUser;
import cn.bmob.v3.datatype.BmobFile;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.UpdateListener;
import cn.bmob.v3.listener.UploadFileListener;

public class UserDataEditing extends AppCompatActivity implements View.OnClickListener{
    private static final String TAG = "UserDataEditing";
    private ImageView userImage;
    private EditText userName;
    private EditText userAge;
    private EditText userSex;
    private Button userSetting;
    private EditText userGrade;

    private Dialog dialog;
    private View inflate;
    private TextView choosePhoto;
    private TextView takePhoto;

    public static final int TAKE_PHOTO = 1;//识别图形启动相机标识
    public static final int CHOOSE_PHOTO = 2;//识别图形启动相册标识
    // 相机拍照后图像的uri
    private Uri imageUri;

    UtilForPicture utilForPicture = UtilForPicture.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_data_editing);
        initControl();
        // 获得用户资料
        MyUser user = BmobUser.getCurrentUser(MyUser.class);
        userName.setText(user.getUsername());
        userSex.setText(user.getSex() + " ");
        userAge.setText(user.getAge() + " ");
        userGrade.setText(user.getGrade() + "");
        Glide.with(UserDataEditing.this)
                .load(user.getImage())
                .error(R.drawable.dog)
                .placeholder(R.drawable.dog)
                .into(userImage) ;

        // 当点击完成编辑按钮时
        userSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateUser();
                try {
                    Toast.makeText(UserDataEditing.this, "请稍等，正在更新您的用户信息",Toast.LENGTH_LONG).show();
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                finish();
            }
        });

    }
    /**
     * 更新用户操作并同步更新本地的用户信息
     */
    private void updateUser() {
        final MyUser user = BmobUser.getCurrentUser(MyUser.class);
        user.setAge(userAge.getText().toString());
        user.setSex(userSex.getText().toString());
        user.setGrade(userGrade.getText().toString());
        user.setUsername(userName.getText().toString());
        user.update(new UpdateListener() {
            @Override
            public void done(BmobException e) {
                if (e == null) {
                    Log.e(TAG, "更新用户信息成功");
                } else {
                    Log.e(TAG, "更新用户信息失败");
                    Log.e("error", e.getMessage());
                }
            }
        });
    }


    /**
     * 初始化控件
     */
    private void initControl(){
        userImage = findViewById(R.id.riv_head_portrait_setting);
        userImage.setOnClickListener(this);
        userName = findViewById(R.id.ev_nicheng_setting);
        userAge = findViewById(R.id.ev_nianling_setting);
        userSex = findViewById(R.id.ev_xingbie_setting);
        userGrade = findViewById(R.id.ev_niaji_setting);
        userSetting = findViewById(R.id.setting_success);
    }
    /**
     * 返回上一个界面
     * @param view
     */
    public void goBack(View view){
        finish();
    }
    /**
     * 更改头像的弹窗
     */
    public void changeUserHead(){
        dialog = new Dialog(this, R.style.ActionSheetDialogStyle);
        //填充对话框的布局
        inflate = LayoutInflater.from(this).inflate(R.layout.dialog_layout, null);
        //初始化控件
        choosePhoto = (TextView) inflate.findViewById(R.id.choosePhoto);
        takePhoto = (TextView) inflate.findViewById(R.id.takePhoto);
        choosePhoto.setOnClickListener(this);
        takePhoto.setOnClickListener(this);
        //将布局设置给Dialog
        dialog.setContentView(inflate);
        //获取当前Activity所在的窗体
        Window dialogWindow = dialog.getWindow();
        //设置Dialog从窗体底部弹出
        dialogWindow.setGravity( Gravity.BOTTOM);
        //获得窗体的属性
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        lp.y = 20;//设置Dialog距离底部的距离
        // 将属性设置给窗体
        dialogWindow.setAttributes(lp);
        dialog.show();//显示对话框
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.takePhoto:
                //创建FIle对象，用于保存拍照后的图片
                File outputImage = new File(getExternalCacheDir(),
                        "output_image.jpg");
                try {
                    //如果这张图片存在就将其删除
                    if(outputImage.exists()){
                        outputImage.delete();
                    }
                    outputImage.createNewFile();
                }catch(Exception e){
                    e.printStackTrace();
                }
                if(Build.VERSION.SDK_INT >= 24){
                    //获取图片路径
                    imageUri = FileProvider.getUriForFile(UserDataEditing.this,"com.example.zhuhaipeng.myapplication.provider",outputImage);
                }else{
                    imageUri = Uri.fromFile(outputImage);
                }
                //启动相机程序
                Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                intent.putExtra(MediaStore.EXTRA_OUTPUT,imageUri);
                //运行app时确认已经授权打开相机
                startActivityForResult(intent,TAKE_PHOTO);
                dialog.dismiss();
                break;
            case R.id.choosePhoto:
                // 是否获得拍照权限
                if(ContextCompat.checkSelfPermission(UserDataEditing.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED){
                    ActivityCompat.requestPermissions(UserDataEditing.this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},1);
                } else {
                    openAlbum();
                }

            case R.id.riv_head_portrait_setting:
                Log.e(TAG, "onClick:点击了头像 " );
                changeUserHead();

            default:
                break;
        }

    }
    private void openAlbum(){
        Intent intent = new Intent("android.intent.action.GET_CONTENT");
        intent.setType("image/*");
        startActivityForResult(intent,CHOOSE_PHOTO);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        dialog.dismiss();
        switch (requestCode) {
            case TAKE_PHOTO: // 选择打开相机照相
                if(resultCode == RESULT_OK){
                    try{
                        Bitmap bitmap = utilForPicture.comp(BitmapFactory
                                .decodeStream(getContentResolver().openInputStream(imageUri)));
                        Glide.with(UserDataEditing.this)
                                .load(bitmap)
                                .into(userImage);
                        uploding(imageUri.toString());
                    }catch (FileNotFoundException e){
                        Log.e(TAG, "onActivityResult: 没获取到图片" );
                        Toast.makeText(UserDataEditing.this,"对不起，您的机型暂未完全适配，未获取到图片！",Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                }
                break;

            case CHOOSE_PHOTO: // 从相册中选择
                if(resultCode == RESULT_OK){
                    //判断手机系统版本号
                    if(Build.VERSION.SDK_INT >= 19){
                        //4.4及以上手机系统使用这个方法处理图片
                        handleImageOnKitKat(UserDataEditing.this,  data);
                    } else {
                        //4.4以下手机系统使用这个方法处理图片
                        handleImageBeforKitKat(UserDataEditing.this, data);
                    }
                }
                break;
            default:
                break;
        }
    }

    private void uploding(String uri) {
        //上传头像
        final String picPath = uri; // 图片在手机中的地址
        final BmobFile bmobFile = new BmobFile(new File(picPath));
        bmobFile.uploadblock(new UploadFileListener() {
            @Override
            public void done(BmobException e) {
                if(e==null){
                    //bmobFile.getFileUrl()--返回的上传文件的完整地址
                    MyUser myUser1 = new MyUser();
                    //  将上传的头像文件url地址存在用户的头像地址信息中，通过Glide拉取
                    myUser1.setImage(bmobFile.getFileUrl());
//                    myUser1.setUserImage(picPath);
                    MyUser userInfo = BmobUser.getCurrentUser(MyUser.class);
                    myUser1.update(userInfo.getObjectId(),new UpdateListener() {
                        @Override
                        public void done(BmobException e) {
                            if(e==null){
                                Log.e(TAG, "更新用户信息成功");
                            }else{
                                Log.e(TAG, "更新用户信息失败");
                            }
                        }
                    });
                }else{
                    Log.e(TAG, "上传文件失败");
                }

            }
            @Override
            public void onProgress(Integer value) {
                // 返回的上传进度（百分比）
            }
        });
    }

    @TargetApi(19)
    public  void handleImageOnKitKat(UserDataEditing userInfo, Intent data){

        String imagePath = null;
        Uri uri = data.getData();
        if(DocumentsContract.isDocumentUri(userInfo,uri)){
            //如果时document类型的Uri，则通过document id处理
            String docId = DocumentsContract.getDocumentId(uri);
            if("com.android.providers.media.documents".equals(uri.getAuthority())){
                String id = docId.split(":")[1]; //解析出数字格式的id
                String selection = MediaStore.Images.Media._ID +"="+id;
                imagePath = getImagePath(userInfo, MediaStore.Images.Media.EXTERNAL_CONTENT_URI,selection);
            } else if("com.android.providers.downloads.documents".equals(uri.getAuthority())){
                Uri contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"),Long.valueOf(docId));
                imagePath = getImagePath(userInfo, contentUri,null);
            }
        } else if ("content".equalsIgnoreCase(uri.getScheme())){
            //如果是content类型的Uri,则使用普通方式处理
            imagePath = getImagePath(userInfo, uri,null);
        } else if("file".equalsIgnoreCase(uri.getScheme())){
            // 如果是 file 类型的 Uri,直接获取图片路径
            imagePath = uri.getPath();
        }
        Bitmap bitmap = utilForPicture.getimage(imagePath);
        Log.e(TAG, "handleImageOnKitKat: "+bitmap);
        Log.e(TAG, "handleImageOnKitKat: "+userImage );
        Glide.with(UserDataEditing.this)
                .load(bitmap)
                .into(userImage);
        uploding(imagePath);
    }

    public void handleImageBeforKitKat(UserDataEditing userInfo, Intent data){
        Uri uri = data.getData();
        String imagePath = getImagePath(userInfo, uri,null);
        Bitmap bitmap = utilForPicture.getimage(imagePath);
        Glide.with(UserDataEditing.this)
                .load(bitmap)
                .into(userImage);
        uploding(imagePath);
    }

    /**
     * 获得图片路径
     * @param uri
     * @param selection
     * @return
     */
    private String getImagePath(UserDataEditing userInfo, Uri uri,String selection){
        String path = null;
        //通过Uri和selection来获取真实的图片路径
        Cursor cursor = userInfo.getContentResolver().query(uri,null,selection,null,null);
        if(cursor != null){
            if(cursor.moveToFirst()){
                path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
            }
            cursor.close();
        }
        return path;
    }
}
