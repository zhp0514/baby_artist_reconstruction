package com.example.zhuhaipeng.babypainter.util;

import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.example.zhuhaipeng.babypainter.FragmentModules.ScrawlFragment;
import com.example.zhuhaipeng.babypainter.MainFunctionModules.Result;
import com.example.zhuhaipeng.babypainter.bean.GetJsonRootBean;
import com.example.zhuhaipeng.babypainter.bean.InputImage;
import com.example.zhuhaipeng.babypainter.bean.InputText;
import com.example.zhuhaipeng.babypainter.bean.JsonRootBean;
import com.example.zhuhaipeng.babypainter.bean.Perception;
import com.example.zhuhaipeng.babypainter.bean.Results;
import com.example.zhuhaipeng.babypainter.bean.UserInfo;
import com.example.zhuhaipeng.babypainter.bean.Values;
import com.example.zhuhaipeng.babypainter.confi.Config;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * author:Created by ZhangPengFei.
 * data: 2017/12/28
 * http工具类
 */
public class HttpUtils {
    private static final int OKHTTP_GET_SUCCESS = 0;
    private static final int OKHTTP_GET_FAIL = 1;
    private static final String TAG = "HttpUtils";
    private static String responseData;
    // 保存传递过来的ScrawlFragment对象，用于回调
    private static ScrawlFragment mScrawlFragment = null;
    //接收OkHttp中发送的Message，根据不同的Message进行不同的UI操作
    private  static Handler  handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what){
                case OKHTTP_GET_SUCCESS:
                    getJsonData(responseData);
                    break;
                case OKHTTP_GET_FAIL:
                    Log.e(TAG, "网络不佳！！！");
                    break;
                default:
                    break;
            }
        }
    };

    /**
     * 发送消息到服务器
     *
     * @param message ：发送的消息
     * @return：消息对象
     */
    public static void sendMessage(ScrawlFragment scrawlFragment, String message, int type) {
        mScrawlFragment = scrawlFragment;
        JsonRootBean jsonRootBean = setMessage(message, type);
        Gson gson1 = new Gson();
        String jsonText = gson1.toJson(jsonRootBean, JsonRootBean.class);
        Log.e(TAG, "json："+jsonText);
        getDataPostJsonAsync(jsonText);
    }

    /**
     * 从服务器获得json数据进行解析
     * @param responseData
     */
    private static void getJsonData(String responseData){
        String result;
        if(responseData.length() < 200){
            result = responseData;
        }else {
            responseData = responseData.substring(114);
            result = '{' + responseData;
            Log.e(TAG, "getJsonData:-------> " + result);
        }
        GetJsonRootBean getJsonRootBean;
        Gson gson = new Gson();
        getJsonRootBean = gson.fromJson(result, GetJsonRootBean.class);
        List<Results> list = getJsonRootBean.getResults();
        for (Results result1 : list) {
            Values values = result1.getValues();
            String t = values.getText();
            Log.e(TAG, "getJsonData: " + t);
            mScrawlFragment.getDataFromTuling(t);
        }
    }
    /**
     * 向服务器发起json数据请求
     * @param jsonStr
     */
    private static void getDataPostJsonAsync(String jsonStr) {
        OkHttpClient client =  new OkHttpClient();
        //设置数据类型为JSON类型
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        //JSON格式数据
        RequestBody requestBody = RequestBody.create(JSON, jsonStr);
        Request request = new Request.Builder()
                .url(Config.URL_KEY)
                .post(requestBody)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Message message = new Message();
                message.what = OKHTTP_GET_FAIL;
                Log.v("onFailure", message.what + "");
                handler.sendMessage(message);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                //response.isSuccessful()这个布尔值代表的是，你使用Post请求过后，返回时是否成功
                //比如我们发送一个错误参数的Post请求，如果添加了response.isSuccessful()，那么他的条件语句不会触发，即response.isSuccessful() == false。
//              if(response.isSuccessful()){
                responseData = response.body().string();
                Log.e(TAG, "onResponse:xxxx " + responseData);
                Message message = new Message();
                message.what = OKHTTP_GET_SUCCESS;
                handler.sendMessage(message);
            }

        });
    }

    private static JsonRootBean setMessage(String message, int type){
        JsonRootBean jsonRootBean = new JsonRootBean();
        Perception perception = new Perception();
        InputImage inputImage = new InputImage();
        InputText inputText = new InputText();
        UserInfo userInfo = new UserInfo();
        jsonRootBean.setReqType(type);
        jsonRootBean.setPerception(perception);
        jsonRootBean.setUserInfo(userInfo);
        perception.setInputText(inputText);
        perception.setInputImage(inputImage);
        inputText.setText(message);
        inputImage.setUrl("");
        userInfo.setApiKey(Config.APP_KEY);
        userInfo.setUserId(Config.USER_ID);

        return jsonRootBean;
    }
}
