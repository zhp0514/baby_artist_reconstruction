package com.example.zhuhaipeng.babypainter.MainFunctionModules;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import com.example.zhuhaipeng.babypainter.R;
import com.example.zhuhaipeng.babypainter.util.InitData;
import com.example.zhuhaipeng.babypainter.util.UtilForPicture;

public class PlayPaintTeaching extends AppCompatActivity implements View.OnClickListener {
    private LinearLayout back;
    private InitData mInitData = null;
    private VideoView show;

    private static final String TAG = "PlayPaintTeaching";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_paint_teaching);
        mInitData = InitData.getInstance();
        initCotrol();
        initData();
    }
    public void initCotrol(){
        back = findViewById(R.id.iv_go_back_in_teaching_video);
        show = findViewById(R.id.vv_teaching_video);

        back.setOnClickListener(this);
        show.setOnClickListener(this);
    }
    public void initData(){
        if(mInitData.getTeachList().get(UtilForPicture.getInfoFromServer()) == null ){
            Toast.makeText(PlayPaintTeaching.this,"暂未添加该种类视频教学",Toast.LENGTH_LONG).show();
        }else{
            show.setVideoPath(mInitData.getTeachList().get(UtilForPicture.getInfoFromServer()));
            //创建MediaController对象
            MediaController mediaController = new MediaController(this);

            //VideoView与MediaController建立关联
            show.setMediaController(mediaController);

            //让VideoView获取焦点
            show.requestFocus();
        }

    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.iv_go_back_in_teaching_video:
                Log.e(TAG, "PlayPaintTeaching销毁");
                finish();
                break;
            case R.id.vv_teaching_video:
                break;
            default:
                break;
        }
    }
}
