package com.example.zhuhaipeng.babypainter.FragmentModules;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.ContentUris;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.zhuhaipeng.babypainter.MainFunctionModules.MainActivityShow;
import com.example.zhuhaipeng.babypainter.R;
import com.example.zhuhaipeng.babypainter.util.HttpUtils;
import com.example.zhuhaipeng.babypainter.util.TTSUtils;
import com.example.zhuhaipeng.babypainter.util.Voice;
import com.google.gson.Gson;
import com.iflytek.cloud.RecognizerResult;
import com.iflytek.cloud.SpeechConstant;
import com.iflytek.cloud.SpeechError;
import com.iflytek.cloud.ui.RecognizerDialog;
import com.iflytek.cloud.ui.RecognizerDialogListener;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.support.v4.provider.FontsContractCompat.FontRequestCallback.RESULT_OK;

/**
 * @author gg_boy
 * @version $Rev$
 * @des ${涂鸦界面}
 * @updateAuther $Auther$
 * @updateDes ${TODO}
 */
public class ScrawlFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = "ScrawlFragment";
    private View view;
    private ImageButton choose_photo;
    private TextView et_show;
    public ImageView showPicture;
    private TTSUtils speaking = null;

    private MainActivityShow mMainActivityShow;
    private HashMap<String, String> picturToScrawl;
    // 是否可以和小新对话
    public static boolean canTalk = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.scrawl_fragment, container, false);
        initCotrol();
        initScrawlPictures();
        return view;
    }

    public HashMap<String, String> getPicturToScrawl() {
        return picturToScrawl;
    }

    public void getMainActivityShow(MainActivityShow mainActivityShow){
        this.mMainActivityShow = mainActivityShow;
    }
    /**
     * 初始化ScrawlFragment中的控件
     */
    public void initCotrol(){
        et_show = view.findViewById(R.id.et_show_speak);
        choose_photo = view.findViewById(R.id.ib_choose_photo);

        et_show.setOnClickListener(this);
        choose_photo.setOnClickListener(this);

        showPicture = view.findViewById(R.id.iv_show_in_scrawl);
        showPicture.setOnClickListener(this);

        speaking = new TTSUtils(view.getContext());
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.iv_show_in_scrawl:
                if(canTalk){
                    initSpeech();
                }
                break;
            case R.id.ib_choose_photo:
                Log.e(TAG, "R.id.ib_choose_photo");
                mMainActivityShow.showDialog();
                break;
            default:
                break;
        }
    }

    public void getDataFromTuling(String resultData){
        et_show.setText(resultData);
        speaking.speaking(resultData);
    }

    private static class Holder{
        private final static ScrawlFragment sScrawlFragment = new ScrawlFragment();
    }
    public static ScrawlFragment getInstance(){
        return Holder.sScrawlFragment;
    }

    /**
     * 实现人机对话的方法
     */
    private void initSpeech(){
        //1、初始化窗口
        RecognizerDialog dialog=new RecognizerDialog(view.getContext(),null);
        //2、设置听写参数，详见官方文档
        //识别中文听写可设置为"zh_cn",此处为设置英文听写
        dialog.setParameter(SpeechConstant.LANGUAGE,"zh_cn");
        dialog.setParameter(SpeechConstant.ACCENT,"mandarin");
        //3、开始听写
        dialog.setListener(new RecognizerDialogListener() {
            @Override
            public void onResult(RecognizerResult recognizerResult, boolean b) {
                if(!b){
                    String result=parseVoice(recognizerResult.getResultString());
                    Log.e(TAG, "onResult: "+result.length());
                    if(result.length() < 150){
                        int type = 0;
                        String REGEX = "([苹果]+|[葡萄]+|[桃子]+|[水蜜桃]+|[猕猴桃]+" +
                                "|[樱桃]+|[橘子]+|[香蕉]+|[菠萝]+|[梨子]+|[草莓]+" +
                                "|[茉莉花]+|[郁金香]+|[桂花]+|[桃花]+" +
                                "|[狗]+|[花]+)";
                        Pattern pattern = Pattern.compile(REGEX);
                        Matcher matcher = pattern.matcher(result);
                        HttpUtils.sendMessage(getInstance(), result, 0);
                        if(matcher.find()){
                            String group = matcher.group();
                            Log.e(TAG, "正则匹配结果;------>"+group );
                            choosPictureToShow(group);
                        }
                    }else{ // 如果内容超过150个字，，图灵机器人会报错
                        Toast.makeText(view.getContext(), "暂不支持长对话！！", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            private void choosPictureToShow(String result) {
                Random random = new Random();
                switch (result){
                    case "苹果":
                        Glide.with(view).load(R.mipmap.pingguo).into(showPicture);
                        break;
                    case "葡萄":
                        Glide.with(view).load(R.mipmap.putao).into(showPicture);
                        break;
                    case "桃子":
                        Glide.with(view).load(R.mipmap.taozi).into(showPicture);
                    case "水蜜桃":
                        Glide.with(view).load(R.mipmap.taozi).into(showPicture);
                    case "猕猴桃":
                        Glide.with(view).load(R.mipmap.taozi).into(showPicture);
                        break;
                    case "樱桃":
                        Glide.with(view).load(R.mipmap.yingtao).into(showPicture);
                        break;
                    case "橘子":
                        Glide.with(view).load(R.mipmap.orange).into(showPicture);
                        break;
                    case "香蕉":
                        Glide.with(view).load(R.mipmap.banan).into(showPicture);
                        break;
                    case "菠萝":
                        Glide.with(view).load(R.mipmap.boluo).into(showPicture);
                        break;
                    case "梨子":
                        Glide.with(view).load(R.mipmap.lizi).into(showPicture);
                        break;
                    case "草莓":
                        Glide.with(view).load(R.mipmap.caomei).into(showPicture);
                        break;
                    case "茉莉花":
                        Glide.with(view).load(R.mipmap.molihua).into(showPicture);
                        break;
                    case "郁金香":
                        Glide.with(view).load(R.mipmap.yujinxiang).into(showPicture);
                        break;
                    case "桂花":
                        Glide.with(view).load(R.mipmap.guihua).into(showPicture);
                        break;
                    case "桃花":
                        Glide.with(view).load(R.mipmap.taohua).into(showPicture);
                        break;
                    case "狗":

                        int choose = random.nextInt(10);
                        if(choose <= 3){
                            Glide.with(view).load(R.mipmap.jingmao).into(showPicture);
                        }else if (choose <= 6){
                            Glide.with(view).load(R.mipmap.hashiqi).into(showPicture);
                        }else{
                            Glide.with(view).load(R.mipmap.caiquan).into(showPicture);
                        }
                        break;
                    case "花":
                        choose = random.nextInt(12);
                        if(choose <= 3){
                            Glide.with(view).load(R.mipmap.yujinxiang).into(showPicture);
                        }else if (choose <= 6){
                            Glide.with(view).load(R.mipmap.molihua).into(showPicture);
                        }else if(choose <= 9){
                            Glide.with(view).load(R.mipmap.taohua).into(showPicture);
                        }else{
                            Glide.with(view).load(R.mipmap.taohua).into(showPicture);
                        }
                        break;
                    default:
//                        Toast.makeText(view.getContext(),
//                                "服务器暂未找到符合该描述的图片，请见谅", Toast.LENGTH_LONG).show();
                        break;
                }
            }

            @Override
            public void onError(SpeechError speechError) {

            }
        });
        dialog.show();
    }
    //解析Gson对象
    public String parseVoice(String resultString) {
        Gson gson =  new Gson();
        Voice voiceBean = gson.fromJson(resultString, Voice.class);

        StringBuffer sb = new StringBuffer();
        ArrayList<Voice.WSBean> ws = voiceBean.ws;
        for (Voice.WSBean wsBean : ws) {
            String word = wsBean.cw.get(0).w;
            sb.append(word);
        }
        return sb.toString();
    }

    @TargetApi(19)
    public  void handleImageOnKitKat(Intent data){
        String imagePath = null;
        Uri uri = data.getData();
        if(DocumentsContract.isDocumentUri(view.getContext(),uri)){
            //如果时document类型的Uri，则通过document id处理
            String docId = DocumentsContract.getDocumentId(uri);
            if("com.android.providers.media.documents".equals(uri.getAuthority())){
                String id = docId.split(":")[1]; //解析出数字格式的id
                String selection = MediaStore.Images.Media._ID +"="+id;
                imagePath = getImagePath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,selection);
            } else if("com.android.providers.downloads.documents".equals(uri.getAuthority())){
                Uri contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"),Long.valueOf(docId));
                imagePath = getImagePath(contentUri,null);
            }
        } else if ("content".equalsIgnoreCase(uri.getScheme())){
            //如果是content类型的Uri,则使用普通方式处理
            imagePath = getImagePath(uri,null);
        } else if("file".equalsIgnoreCase(uri.getScheme())){
            // 如果是 file 类型的 Uri,直接获取图片路径
            imagePath = uri.getPath();
        }
        displayImage(imagePath);
    }
    private String getImagePath(Uri uri,String selection){
        String path = null;
        //通过Uri和selection来获取真实的图片路径
        Cursor cursor = mMainActivityShow.getContentResolver().query(uri,null,selection,null,null);
        if(cursor != null){
            if(cursor.moveToFirst()){
                path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
            }
            cursor.close();
        }
        return path;
    }
    public void handleImageBeforKitKat(Intent data){
        Uri uri = data.getData();
        String imagePath = getImagePath(uri,null);
        displayImage(imagePath);
    }
    /**
     * 显示图片
     * @param imagePath 图片路径
     */
    private void  displayImage(String imagePath){
        if(imagePath != null){
            //对图片进行压缩
            Random random = new Random();
            Integer pos = (random.nextInt(10) + 1);
            String s = pos.toString();
            //涂鸦画面
            Glide.with(view.getContext())
                    .load(picturToScrawl.get(pos))
                    .placeholder(R.drawable.dog)
                    .into(showPicture);
            MainActivityShow.kqwSpeechCompound.speaking("点击图片还可以与小新说话哦");
        }else {
            Toast.makeText(mMainActivityShow,"获取图片失败",Toast.LENGTH_LONG).show();
        }
    }
    /**
     * 初始化涂鸦显示的图片数据
     * @return
     */
    private void initScrawlPictures() {
        picturToScrawl = new HashMap<>();
        picturToScrawl.put("10","http://bmob-cdn-19476.b0.upaiyun.com/2018/11/09/4a0f9e0340afc5f1806bfdcdc781a1c8.jpg");
        picturToScrawl.put("9","http://bmob-cdn-19476.b0.upaiyun.com/2018/11/09/3776c7004007971f8007ba0e4c7c2317.jpeg");
        picturToScrawl.put("8","http://bmob-cdn-19476.b0.upaiyun.com/2018/11/09/ebecc55e40c44590809645c54bde2309.jpg");
        picturToScrawl.put("7","http://bmob-cdn-19476.b0.upaiyun.com/2018/11/09/4bcb32b540a01c11808b1ea2276c2ec0.jpg");
        picturToScrawl.put("6","http://bmob-cdn-19476.b0.upaiyun.com/2018/11/09/b5cebe05405bb7558035e1374f0a51f5.jpg");
        picturToScrawl.put("5","http://bmob-cdn-19476.b0.upaiyun.com/2018/11/09/fb4479ad4062f771807570e7fc75263f.jpg");
        picturToScrawl.put("4","http://bmob-cdn-19476.b0.upaiyun.com/2018/11/09/b9d167f5402b838b8067171eafd188b0.jpg");
        picturToScrawl.put("3","http://bmob-cdn-19476.b0.upaiyun.com/2018/11/09/8a7f6e4240f7bdd880a48a35a9188bc3.gif");
        picturToScrawl.put("2","http://bmob-cdn-19476.b0.upaiyun.com/2018/11/09/4312cfd04052b32480c988f92e7faedd.jpg");
        picturToScrawl.put("1","http://bmob-cdn-19476.b0.upaiyun.com/2018/11/09/98abd6e44069ee3780b024e70ebb6b8f.jpg");
    }
}
