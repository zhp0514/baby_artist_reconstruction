package com.example.zhuhaipeng.babypainter.VideoTeach;

/**
 * 用于画画教学中的显示的数据类
 */
public class VideoForPaint {
    private String name;
    private int id;

    public VideoForPaint(String name,int id){
        this.name = name;
        this.id = id;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
