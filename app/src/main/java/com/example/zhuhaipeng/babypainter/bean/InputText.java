package com.example.zhuhaipeng.babypainter.bean; /**
 * Copyright 2019 bejson.com
 */

/**
 * Auto-generated: 2019-04-19 11:18:53
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class InputText {

    private String text;
    public void setText(String text) {
        this.text = text;
    }
    public String getText() {
        return text;
    }

}