package com.example.zhuhaipeng.babypainter.util;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Message;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.bumptech.glide.Glide;
import com.example.zhuhaipeng.babypainter.AllBmobClass.MyUser;
import com.example.zhuhaipeng.babypainter.MainFunctionModules.MainActivityShow;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import cn.bmob.v3.BmobUser;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * @author gg_boy
 * @version $Rev$
 * @des ${TODO}
 * @updateAuther gg_boy
 * @updateDes ${TODO}
 */
public class UtilForPicture {
    private static final String TAG = "UtilForPicture";
    private static String infoFromServer = "";

    public static String getInfoFromServer() {
        return infoFromServer;
    }

    private static class Holder{
        private final static UtilForPicture UTIL_FOR_PICTURE = new UtilForPicture();
    }
    public static UtilForPicture getInstance(){
        return Holder.UTIL_FOR_PICTURE;
    }
    /**
     *  图片按比例大小压缩方法（根据Bitmap图片压缩）
     * @param image
     * @return
     */
    public Bitmap comp(Bitmap image) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        if( baos.toByteArray().length / 1024>1024) {//判断如果图片大于1M,进行压缩避免在生成图片（BitmapFactory.decodeStream）时溢出
            baos.reset();//重置baos即清空baos
            image.compress(Bitmap.CompressFormat.JPEG, 50, baos);//这里压缩50%，把压缩后的数据存放到baos中
        }
        ByteArrayInputStream isBm = new ByteArrayInputStream(baos.toByteArray());
        BitmapFactory.Options newOpts = new BitmapFactory.Options();
        //开始读入图片，此时把options.inJustDecodeBounds 设回true了
        newOpts.inJustDecodeBounds = true;
        Bitmap bitmap = BitmapFactory.decodeStream(isBm, null, newOpts);
        newOpts.inJustDecodeBounds = false;
        int w = newOpts.outWidth;
        int h = newOpts.outHeight;
        //现在主流手机比较多是800*480分辨率，所以高和宽我们设置为
        float hh = 800f;//这里设置高度为800f
        float ww = 480f;//这里设置宽度为480f
        //缩放比。由于是固定比例缩放，只用高或者宽其中一个数据进行计算即可
        int be = 1;//be=1表示不缩放
        if (w > h && w > ww) {//如果宽度大的话根据宽度固定大小缩放
            be = (int) (newOpts.outWidth / ww);
        } else if (w < h && h > hh) {//如果高度高的话根据宽度固定大小缩放
            be = (int) (newOpts.outHeight / hh);
        }
        if (be <= 0)
            be = 1;
        newOpts.inSampleSize = be;//设置缩放比例
        //重新读入图片，注意此时已经把options.inJustDecodeBounds 设回false了
        isBm = new ByteArrayInputStream(baos.toByteArray());
        bitmap = BitmapFactory.decodeStream(isBm, null, newOpts);
        return compressImage(bitmap);//压缩好比例大小后再进行质量压缩
    }
    /**
     *  对于图片进行质量压缩
     * @param image
     * @return
     */
    public Bitmap compressImage(Bitmap image) {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 100, baos);//质量压缩方法，这里100表示不压缩，把压缩后的数据存放到baos中
        int options = 100;
        while ( baos.toByteArray().length / 1024>100) { //循环判断如果压缩后图片是否大于100kb,大于继续压缩
            baos.reset();//重置baos即清空baos
            image.compress(Bitmap.CompressFormat.JPEG, options, baos);//这里压缩options%，把压缩后的数据存放到baos中
            options -= 10;//每次都减少10
        }
        ByteArrayInputStream isBm = new ByteArrayInputStream(baos.toByteArray());//把压缩后的数据baos存放到ByteArrayInputStream中
        Bitmap bitmap = BitmapFactory.decodeStream(isBm, null, null);//把ByteArrayInputStream数据生成图片
        return bitmap;
    }

    /**
     * 向服务器发起识别图片请求
     */
    public void requestConnect(MainActivityShow context, String access_token, String base64String){

        //拼接网址
        String url = "https://aip.baidubce.com/rpc/2.0/ai_custom/v1/classification/paint";
        String url1 = url + "?access_token=" + access_token;

        //创建json数据
        HashMap<String,String> map = new HashMap<>();
        map.put("image",base64String);
        //将参数转为一个json
        JSONObject jsonObject = new JSONObject();
        if(null != map){
            for (String s : map.keySet()) {
                jsonObject.put(s,map.get(s));
            }
        }
        //创建一个RequestBody
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8")
                , jsonObject.toJSONString());
        //创建okHttpClient对象
        OkHttpClient client = new OkHttpClient();
        //创建一个Request
        final Request request1 = new Request.Builder()
                .url(url1)
                .header("Content-Type","application/json")
                .post(requestBody)
                .build();
        getResultInfo(context, client, request1);
    }

    private void getResultInfo(final MainActivityShow mainActivityShow, OkHttpClient client, Request request1) {
        Log.e(TAG, "requestConnect: 向服务器发起请求");
        //new call
        Call call = client.newCall(request1);
        //请求加入调度
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e)
            {

            }
            @Override
            public void onResponse(Call call, Response response)  {
                try{
                    MyUser myUser = BmobUser.getCurrentUser(MyUser.class);
                    String htmlStr =  response.body().string();
                    JSONObject object = JSON.parseObject(htmlStr);
                    String result = object.getString("results");
                    JSONArray array = JSON.parseArray(result);
                    Log.e(TAG, "onResponse: "+array );
                    JSONObject objectWithJson = array.getJSONObject(0);
                    infoFromServer = objectWithJson.getString("name");
                    Log.e(TAG, "服务器返回的结果是"+infoFromServer);
                    mainActivityShow.getRecognitionResult(infoFromServer);
                }catch (IOException e){
                    Log.e(TAG, "onResponse: Call这里出错了" );
                }
            }
        });
    }

    /**
     * Bitmap 通过Base64 转换为字符串
     * @param bitmap
     * @return
     */
    public String getBitmapStrBase64(Bitmap bitmap){
        //        Log.e("bit+++++++",""+bitmap);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
        byte[] bytes = bos.toByteArray();
        String string = Base64.encodeToString(bytes, Base64.DEFAULT);
        return string;
    }

    @TargetApi(19)
    public  String handleImageOnKitKat(MainActivityShow mainActivityShow, String access_token, Intent data){
        String imagePath = null;
        Uri uri = data.getData();
        if(DocumentsContract.isDocumentUri(mainActivityShow,uri)){
            //如果时document类型的Uri，则通过document id处理
            String docId = DocumentsContract.getDocumentId(uri);
            if("com.android.providers.media.documents".equals(uri.getAuthority())){
                String id = docId.split(":")[1]; //解析出数字格式的id
                String selection = MediaStore.Images.Media._ID +"="+id;
                imagePath = getImagePath(mainActivityShow, MediaStore.Images.Media.EXTERNAL_CONTENT_URI,selection);
            } else if("com.android.providers.downloads.documents".equals(uri.getAuthority())){
                Uri contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"),Long.valueOf(docId));
                imagePath = getImagePath(mainActivityShow, contentUri,null);
            }
        } else if ("content".equalsIgnoreCase(uri.getScheme())){
            //如果是content类型的Uri,则使用普通方式处理
            imagePath = getImagePath(mainActivityShow, uri,null);
        } else if("file".equalsIgnoreCase(uri.getScheme())){
            // 如果是 file 类型的 Uri,直接获取图片路径
            imagePath = uri.getPath();
        }
        Bitmap bitmap = getimage(imagePath);
        String base64String = getBitmapStrBase64(bitmap);
        requestConnect(mainActivityShow, access_token, base64String);
        return imagePath;
    }

    public String handleImageBeforKitKat(MainActivityShow mainActivityShow,String access_token, Intent data){
        Uri uri = data.getData();
        String imagePath = getImagePath(mainActivityShow, uri,null);
        Bitmap bitmap = getimage(imagePath);
        String base64String = getBitmapStrBase64(bitmap);
        requestConnect(mainActivityShow, access_token, base64String);
        return imagePath;
    }

    /**
     * 获得图片路径
     * @param uri
     * @param selection
     * @return
     */
    private String getImagePath(MainActivityShow mainActivityShow, Uri uri,String selection){
        String path = null;
        //通过Uri和selection来获取真实的图片路径
        Cursor cursor = mainActivityShow.getContentResolver().query(uri,null,selection,null,null);
        if(cursor != null){
            if(cursor.moveToFirst()){
                path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
            }
            cursor.close();
        }
        return path;
    }

    /**
     * 图片按比例大小压缩方法（根据路径获取图片并压缩）
     * @param srcPath
     * @return
     */
    public Bitmap getimage(String srcPath) {
        BitmapFactory.Options newOpts = new BitmapFactory.Options();
        //开始读入图片，此时把options.inJustDecodeBounds 设回true了
        newOpts.inJustDecodeBounds = true;
        Bitmap bitmap = BitmapFactory.decodeFile(srcPath,newOpts);//此时返回bm为空

        newOpts.inJustDecodeBounds = false;
        int w = newOpts.outWidth;
        int h = newOpts.outHeight;
        //现在主流手机比较多是800*480分辨率，所以高和宽我们设置为
        float hh = 800f;//这里设置高度为800f
        float ww = 480f;//这里设置宽度为480f
        //缩放比。由于是固定比例缩放，只用高或者宽其中一个数据进行计算即可
        int be = 1;//be=1表示不缩放
        if (w > h && w > ww) {//如果宽度大的话根据宽度固定大小缩放
            be = (int) (newOpts.outWidth / ww);
        } else if (w < h && h > hh) {//如果高度高的话根据宽度固定大小缩放
            be = (int) (newOpts.outHeight / hh);
        }
        if (be <= 0)
            be = 1;
        newOpts.inSampleSize = be;//设置缩放比例
        //重新读入图片，注意此时已经把options.inJustDecodeBounds 设回false了
        bitmap = BitmapFactory.decodeFile(srcPath, newOpts);
        return compressImage(bitmap);//压缩好比例大小后再进行质量压缩
    }


}
