package com.example.zhuhaipeng.babypainter.bean; /**
 * Copyright 2019 bejson.com
 */


import com.example.zhuhaipeng.babypainter.bean.Values;

/**
 * Auto-generated: 2019-04-19 12:52:53
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Results {

    private int groupType;
    private String resultType;
    private Values values;
    public void setGroupType(int groupType) {
        this.groupType = groupType;
    }
    public int getGroupType() {
        return groupType;
    }

    public void setResultType(String resultType) {
        this.resultType = resultType;
    }
    public String getResultType() {
        return resultType;
    }

    public void setValues(Values values) {
        this.values = values;
    }
    public Values getValues() {
        return values;
    }

}