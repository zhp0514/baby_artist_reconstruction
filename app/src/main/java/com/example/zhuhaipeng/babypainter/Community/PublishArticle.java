package com.example.zhuhaipeng.babypainter.Community;

import android.content.ContentUris;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.zhuhaipeng.babypainter.MainFunctionModules.MainActivityShow;
import com.example.zhuhaipeng.babypainter.R;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;

import java.util.List;

public class PublishArticle extends AppCompatActivity implements View.OnClickListener {
    private ImageView back;
    private ImageView publish;
    private ImageView choosePhoto;
    private TextView article;
    private static final String TAG = "PublishArticle";
    private  static  final  int REQUEST_CODE_CHOOSE = 23;//定义请求码常量
    private String[] filePaths;
    private UriAdapter mAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_publish_article);

        RecyclerView recyclerView =(RecyclerView) findViewById(R.id.publish_recyclerView);
        recyclerView.setLayoutManager(new GridLayoutManager(this,3));
        recyclerView.setAdapter(mAdapter = new UriAdapter());

        back = (ImageView) findViewById(R.id.go_back);
        publish = (ImageView) findViewById(R.id.publish);
        choosePhoto = (ImageView) findViewById(R.id.choosePhoto);
        article = (TextView) findViewById(R.id.article);

        back.setOnClickListener(this);
        publish.setOnClickListener(this);
        choosePhoto.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.go_back:
                finish();
                break;
            case R.id.publish:
                Log.e(TAG, "点击了发表文章" );
                Intent intent1 = new Intent(PublishArticle.this,SkipActivity.class);
                intent1.putExtra("String[]",filePaths);
                intent1.putExtra("article",article.getText().toString());
                startActivity(intent1);
                finish();
                break;
            case R.id.choosePhoto:
                Matisse.from(PublishArticle.this)
                        .choose(MimeType.allOf())
                        .theme(R.style.Matisse_Dracula)
                        .countable(true)
                        .maxSelectable(9)
                        .imageEngine(new MyGlideEngine())
                        .forResult(REQUEST_CODE_CHOOSE);
                break;
            default:
                break;
        }
//        mAdapter.setData(null);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String imagePath = "";
        if (requestCode == REQUEST_CODE_CHOOSE && resultCode == RESULT_OK) {
            mAdapter.setData(Matisse.obtainResult(data));
            List<Uri> imgs = Matisse.obtainResult(data);
            filePaths = new String[imgs.size()];
            for(int i =0; i < imgs.size();i++){
                if(Build.VERSION.SDK_INT >= 19) {
                    if (DocumentsContract.isDocumentUri(this, imgs.get(i))) {
                        //如果时document类型的Uri，则通过document id处理
                        String docId = DocumentsContract.getDocumentId(imgs.get(i));
                        if ("com.android.providers.media.documents".equals(imgs.get(i).getAuthority())) {
                            String id = docId.split(":")[1]; //解析出数字格式的id
                            String selection = MediaStore.Images.Media._ID + "=" + id;
                            imagePath = getImagePath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, selection);
                        } else if ("com.android.providers.downloads.documents".equals(imgs.get(i).getAuthority())) {
                            Uri contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), Long.valueOf(docId));
                            imagePath = getImagePath(contentUri, null);
                        }
                    } else if ("content".equalsIgnoreCase(imgs.get(i).getScheme())) {
                        //如果是content类型的Uri,则使用普通方式处理
                        imagePath = getImagePath(imgs.get(i), null);
                    } else if ("file".equalsIgnoreCase(imgs.get(i).getScheme())) {
                        // 如果是 file 类型的 Uri,直接获取图片路径
                        imagePath = imgs.get(i).getPath();
                    }
                    filePaths[i] = imagePath;
                }else{
                     imagePath = getImagePath(imgs.get(i),null);
                    filePaths[i] = imagePath;
                }
            }
        }
    }
    private String getImagePath(Uri uri,String selection){
        String path = null;
        //通过Uri和selection来获取真实的图片路径
        Cursor cursor = getContentResolver().query(uri,null,selection,null,null);
        if(cursor != null){
            if(cursor.moveToFirst()){
                path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
            }
            cursor.close();
        }
        return path;
    }
    private class UriAdapter extends RecyclerView.Adapter<UriAdapter.UriViewHolder> {
        private List<Uri> mUris;
        void setData(List<Uri> uris) {
            mUris = uris;
            notifyDataSetChanged();
        }

        @Override
        public UriViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new UriViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.publish_recyclerview, parent, false));
        }

        @Override
        public void onBindViewHolder(UriViewHolder holder, int position) {
            Glide.with(PublishArticle.this).load(mUris.get(position)).into(holder.mImg);
        }

        @Override
        public int getItemCount() {
            return mUris == null ? 0 : mUris.size();
        }

class UriViewHolder extends RecyclerView.ViewHolder {
    private ImageView mImg;
    UriViewHolder(View contentView) {
        super(contentView);
        mImg = (ImageView) contentView.findViewById(R.id.imgToShowPhoto);
    }
}
    }
}

