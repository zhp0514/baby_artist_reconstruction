package com.example.zhuhaipeng.babypainter.Community;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.example.zhuhaipeng.babypainter.AllBmobClass.MyUser;
import com.example.zhuhaipeng.babypainter.AllBmobClass.Post;
import com.example.zhuhaipeng.babypainter.MainFunctionModules.MainActivityShow;
import com.example.zhuhaipeng.babypainter.R;


import java.util.ArrayList;
import java.util.List;

import cn.bmob.v3.BmobUser;
import cn.bmob.v3.datatype.BmobFile;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.SaveListener;
import cn.bmob.v3.listener.UploadBatchListener;

public class SkipActivity extends AppCompatActivity {
    private static final String TAG = "SkipActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_skip);
        final String[] filePaths = getIntent().getStringArrayExtra("String[]");
        final  String article = getIntent().getStringExtra("article");
        if((null != filePaths && !article.equals(""))||(filePaths != null && article.equals(""))){
            final List<String> list = new ArrayList<String >();
            BmobFile.uploadBatch(filePaths, new UploadBatchListener() {
                @Override
                public void onSuccess(List<BmobFile> files, List<String> urls) {
                    //1、files-上传完成后的BmobFile集合，是为了方便大家对其上传后的数据进行操作，例如你可以将该文件保存到表中
                    //2、urls-上传文件的完整url地址

                    if(urls.size()==filePaths.length){//如果数量相等，则代表文件全部上传完成
                        //do something
                        for(int i = 0;i < filePaths.length;i++){
                            list.add(urls.get(i));
                        }
                        MyUser myUser = BmobUser.getCurrentUser(MyUser.class);
                        //创建帖子信息
                        Post post = new Post();
                        post.setContent(article);
                        post.setPicture(list);
                        //添加一对一关联
                        post.setAuthor(myUser);
                        post.setBrowseNumber(0);
                        post.save(new SaveListener<String>() {
                            @Override
                            public void done(String s, BmobException e) {
                                if(e==null){
                                    Toast.makeText(SkipActivity.this,"发表成功",Toast.LENGTH_SHORT).show();
                                    MyUser myUser = BmobUser.getCurrentUser(MyUser.class);
                                    MainActivityShow.scene = 3;
                                    finish();
                                }else{
                                    Toast.makeText(SkipActivity.this,"发表失败",Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    }
                }
                @Override
                public void onError(int statuscode, String errormsg) {
                    Log.e(TAG, "错误码"+statuscode +",错误描述："+errormsg );
                }
                @Override
                public void onProgress(int curIndex, int curPercent, int total,int totalPercent) {
                    //1、curIndex--表示当前第几个文件正在上传
                    Log.e(TAG, "第"+curIndex+"在上传" );
                    //2、curPercent--表示当前上传文件的进度值（百分比）
                    Log.e(TAG, "第"+curIndex+"上传了百分之"+curPercent );
                    //3、total--表示总的上传文件数
                    Log.e(TAG, "上传了"+total+"文件" );
                    //4、totalPercent--表示总的上传进度（百分比）
                }
            });
        }else if(filePaths == null && !article.equals("")) {
            MyUser myUser = BmobUser.getCurrentUser(MyUser.class);
            //创建帖子信息
            final Post post = new Post();
            post.setContent(article);
            //添加一对一关联
            post.setAuthor(myUser);
            post.setBrowseNumber(0);
            post.save(new SaveListener<String>() {
                @Override
                public void done(String s, BmobException e) {
                    if(e==null){
                        Toast.makeText(SkipActivity.this,"发表成功",Toast.LENGTH_SHORT).show();
                        MyUser myUser = BmobUser.getCurrentUser(MyUser.class);
                        MainActivityShow.scene = 3;
                        finish();
                    }else{
                        Toast.makeText(SkipActivity.this,"发表失败",Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }

    }
}
