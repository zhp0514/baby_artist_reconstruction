package com.example.zhuhaipeng.babypainter.AllBmobClass;

import com.example.zhuhaipeng.babypainter.bean.Comment;

import cn.bmob.v3.BmobObject;

public class Replay extends BmobObject {
    private String content; // 回复的内容
    private MyUser user;
    private Comment comment;

    public Comment getComment() {
        return comment;
    }

    public void setComment(Comment comment) {
        this.comment = comment;
    }

    public MyUser getUser() {
        return user;
    }

    public void setUser(MyUser user) {
        this.user = user;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
