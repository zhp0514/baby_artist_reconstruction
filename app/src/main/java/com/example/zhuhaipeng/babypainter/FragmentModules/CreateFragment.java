package com.example.zhuhaipeng.babypainter.FragmentModules;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.zhuhaipeng.babypainter.MainFunctionModules.DoubleAction;
import com.example.zhuhaipeng.babypainter.MainFunctionModules.MainActivityShow;
import com.example.zhuhaipeng.babypainter.MainFunctionModules.PlayPaintTeaching;
import com.example.zhuhaipeng.babypainter.MainFunctionModules.PlaySongVideo;
import com.example.zhuhaipeng.babypainter.MainFunctionModules.UnityPlayerActivity;
import com.example.zhuhaipeng.babypainter.R;
import com.example.zhuhaipeng.babypainter.util.InitData;

import java.util.List;

/**
 * @author gg_boy
 * @version $Rev$
 * @des ${主页界面}
 * @updateAuther $Auther$
 * @updateDes ${TODO}
 */
public class CreateFragment extends Fragment implements View.OnClickListener{
    public TextView tv_hint = null;
    public ImageButton mImageButton = null;
    public ImageView mImageView = null;
    public ImageView whatYouPaint = null;
    public TextView tv_grade = null;
    public ImageView iv_figure = null;
    public TextView tv_fen = null;
    private static View mView;
    private static final String TAG = "CreateFragment";
    private MainActivityShow mMainActivityShow;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.create_fragment, container, false);
        initControl();
        return mView;
    }

    public void getMainActivity(MainActivityShow mainActivityShow){
        this.mMainActivityShow = mainActivityShow;
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.iv_start_recognition:
                Log.e(TAG, "iv_start_recognition:"+mMainActivityShow.toString());
                mMainActivityShow.recognition();
                break;
            default:
                break;
        }
    }

    private static class Holder{
        @SuppressLint("StaticFieldLeak")
        private final static CreateFragment sCreateFragment = new CreateFragment();
    }
    public static CreateFragment getInstance(){
        return Holder.sCreateFragment;
    }
    public void initControl(){
        mImageView = mView.findViewById(R.id.iv_start_recognition);
        mImageView.setOnClickListener(this);
        tv_hint = mView.findViewById(R.id.tv_hint);
        mImageButton = mView.findViewById(R.id.ib_pronunciation);
        whatYouPaint = mView.findViewById(R.id.iv_whatYouPaint);
        tv_grade = mView.findViewById(R.id.tv_grade);
        iv_figure = mView.findViewById(R.id.iv_figure);
        tv_fen = mView.findViewById(R.id.tv_fen_zi);
    }
    public void Choise(final List<String> values, final List<String> keys, final MainActivityShow _ac, final DoubleAction _action, String _title)
    {
        if(values.size()==0)
        {
            Toast.makeText(_ac, "正好努力加载数据中，请稍等", Toast.LENGTH_SHORT).show();
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(_ac,R.style.MyDialog);
        //builder.setIcon(R.drawable.ic_launcher);
        builder.setTitle(_title);
        //    指定下拉列表的显示数据
        //    设置一个下拉的列表选择项
        builder.setItems(values.toArray(new String[values.size()]), new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                String name = values.get(which);
                if (!name.equals("未选择")) {
                    if(keys==null)
                    {
                        //_action.actionDouble(name,null);
                    }
                    else
                    {
                        switch (keys.get(which)) {
                            case "1":
                                Log.e(TAG, "onClick: " + "视频");
                                MainActivityShow.story = true;
                                Intent toTeachingViedo = new Intent(mView.getContext(), PlayPaintTeaching.class);
                                startActivity(toTeachingViedo);
                                break;
                            case "2":
                                Log.e(TAG, "onClick: " + "儿歌");
                                MainActivityShow.song = true;
                                Intent toPlaySongVideo = new Intent(mView.getContext(), PlaySongVideo.class);
                                startActivity(toPlaySongVideo);
                                break;
                            case "3":
                                Log.e(TAG, "onClick: " + "模型");
//                                MainActivityShow.unity = true;
                                Log.e(TAG, "handleMessage: 打开Unity" );
                                Intent intent = new Intent(_ac, UnityPlayerActivity.class);
                                startActivity(intent);
                                break;
                        }

                        _action.actionDouble(name,keys.get(which));
                    }
                } else {
                    // _action.actionDouble(name,"-1");
                }
            }
        });

        AlertDialog r_dialog = builder.create();
        r_dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        r_dialog.show();
    }
}
