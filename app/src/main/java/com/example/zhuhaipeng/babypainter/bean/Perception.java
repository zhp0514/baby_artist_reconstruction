package com.example.zhuhaipeng.babypainter.bean;

import com.example.zhuhaipeng.babypainter.bean.InputText;

/**
 * Auto-generated: 2019-04-19 23:25:13
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Perception {

    private InputText inputText;
    private InputImage inputImage;
    public void setInputText(InputText inputText) {
        this.inputText = inputText;
    }
    public InputText getInputText() {
        return inputText;
    }

    public void setInputImage(InputImage inputImage) {
        this.inputImage = inputImage;
    }
    public InputImage getInputImage() {
        return inputImage;
    }

}