package com.example.zhuhaipeng.babypainter.MainFunctionModules;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.zhuhaipeng.babypainter.AllBmobClass.MyUser;
import com.example.zhuhaipeng.babypainter.R;

import cn.bmob.v3.BmobUser;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.SaveListener;

public class Register extends AppCompatActivity {
    private EditText user;
    private EditText password;
    private EditText password_again;
    private EditText email;
    private Button sure;
    private static final String TAG = "Register";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        init();
        sure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyUser bu = new MyUser();
                if(!user.getText().toString().isEmpty() &&  !email.getText().toString().isEmpty()){
                    if(password.getText().toString().equals(password_again.getText().toString())){
                        Log.e(TAG, "init:========================== " );
                        bu.setUsername(user.getText().toString());
                        bu.setPassword(password.getText().toString());
                        bu.setEmail(email.getText().toString());
                        //注意：不能用save方法进行注册
                        bu.signUp(new SaveListener<MyUser>() {
                            @Override
                            public void done(MyUser s, BmobException e) {
                                if(e==null){
                                    Toast.makeText(Register.this,"注册成功:",Toast.LENGTH_LONG).show();
                                }else{
                                    Toast.makeText(Register.this, "用户名或邮箱已存在!!", Toast.LENGTH_LONG).show();
                                    Log.e(TAG, "done: "+e.toString());
                                }
                            }
                        });
                    }
                }
                Intent intent = new Intent(Register.this,LogIn.class);
                startActivity(intent);
                finish();
            }
        });
    }
    private void init(){
        user = (EditText)findViewById(R.id.et_user);
        password = (EditText) findViewById(R.id.et_password);
        password_again = (EditText) findViewById(R.id.et_password_again);
        sure = (Button) findViewById(R.id.btn_sure);
        email = (EditText) findViewById(R.id.et_email);
    }
}
