package com.example.zhuhaipeng.babypainter.UserInfo;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.zhuhaipeng.babypainter.AllBmobClass.MyUser;
import com.example.zhuhaipeng.babypainter.R;

import java.util.ArrayList;
import java.util.List;

import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.BmobUser;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;
import de.hdodenhof.circleimageview.CircleImageView;

public class MoreProduct extends AppCompatActivity {
    private static List<String> product = null;
    private static final String TAG = "MoreProduct";
    private  GridView gridView;
    private  int[] images;
    private  String[] ss;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_product);

        product = new ArrayList<>();
        final MyUser myUser = BmobUser.getCurrentUser(MyUser.class);
        BmobQuery<MyUser> userBmobQuery = new BmobQuery<>();
        userBmobQuery.findObjects(new FindListener<MyUser>() {
            @Override
            public void done(List<MyUser> object, BmobException e) {
                if(e==null){
                    Log.i("bmob","查询成功：共" + object.size() + "条数据。");
                    for(MyUser user: object){
                        if(user.getObjectId().equals(myUser.getObjectId())){
                            product = user.getPainted();
                        }
                    }
                }else{
                    Toast.makeText(MoreProduct.this, "与服务器连接失败！！", Toast.LENGTH_LONG).show();
                    Log.i("bmob","失败："+e.getMessage());
                }
                GetStart();
            }
        });


        gridView = (GridView) findViewById(R.id.gv_zuopin);
        images = new int[]{
                R.drawable.p3, R.drawable.p3, R.drawable.p3, R.drawable.p3,
                R.drawable.p3, R.drawable.p3, R.drawable.p3, R.drawable.p3,
                R.drawable.p3, R.drawable.p3, R.drawable.p3, R.drawable.p3,
                R.drawable.p3, R.drawable.p3, R.drawable.p3, R.drawable.p3
        };
         ss = new String[]{
                "1",
                "2",
                "3",
                "4",
                "5",
                "6",
                "7",
                "8",
                "9",
                "10",
                "11",
                "12",
                "13",
                "14",
                "15",
                "16"
        };

    }
    public void returnUpper(View view){
        finish();
    }

    public void GetStart(){
        gridView.setAdapter(new myAdapter(images,this,ss));
    }

    private class myAdapter extends BaseAdapter {
        private int[] images = new int[]{};
        private Context context;
        private String[] ss;
        public myAdapter() {
            super();
        }

        public myAdapter(int[] images, Context context,String[] ss) {
            super();
            this.images = images;
            this.context = context;
            this.ss = ss;
        }

        @Override
        public int getCount() {
            return images.length;
        }

        @Override
        public Object getItem(int arg0) {
            return null;
        }

        @Override
        public long getItemId(int arg0) {
            return 0;
        }

        @Override
        public View getView(int arg0, View arg1, ViewGroup arg2) {

            int resId = images[arg0];
            String name = ss[arg0];

            ViewGroup group =  (ViewGroup) getLayoutInflater().inflate(R.layout.zuopin, null);

            CircleImageView riv = group.findViewById(R.id.imageRound1);
            Log.e(TAG, "getView: "+arg0);
            if (product != null){
                if(product.size() != 0){
                    Log.e(TAG, "作品集大小"+product.size());
                    if(arg0 >= product.size()){
                        riv.setImageResource(resId);
                    }else{
                        Log.e(TAG, "索引"+arg0);
                        Log.e(TAG, "从服务器上获取base64编码进行转码");
                        String s = product.get(arg0);
                        Glide.with(MoreProduct.this).load(s).into(riv);
                    }
                }else{
                    riv.setImageResource(resId);
                }
            }

            TextView textView = (TextView) group.findViewById(R.id.textview1);
            textView.setText(name);
            return group;
        }

    }
    public Bitmap stringtoBitmap(String string) {
        //将字符串转换成Bitmap类型
        Bitmap bitmap = null;
        try {
            byte[] bitmapArray;
            bitmapArray = Base64.decode(string, Base64.DEFAULT);
            bitmap = BitmapFactory.decodeByteArray(bitmapArray, 0, bitmapArray.length);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }

}
