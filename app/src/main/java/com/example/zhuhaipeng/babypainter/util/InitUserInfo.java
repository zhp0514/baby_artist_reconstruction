package com.example.zhuhaipeng.babypainter.util;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.zhuhaipeng.babypainter.AllBmobClass.MyUser;

import cn.bmob.v3.BmobUser;

/**
 * @author gg_boy
 * @version $Rev$
 * @des ${TODO}
 * @updateAuther $Auther$
 * @updateDes ${TODO}
 */
public class InitUserInfo {
    private static final String TAG = "InitUserInfo";
    private String userHeadImage = null;
    private String userName = null;
    private String userEmail = null;

    private static class Holder{
        private final static InitUserInfo INIT_USER_INFO = new InitUserInfo();
    }
    public static InitUserInfo getInstance(){
        return Holder.INIT_USER_INFO;
    }

    public void initData(){
        if (MyUser.isLogin()) { // 这里有问题
            MyUser user = BmobUser.getCurrentUser(MyUser.class);
            Log.e(TAG, "初始化数据成功: " + user.getUsername());
            userHeadImage = user.getImage();
            userName = user.getUsername();
            userEmail = user.getEmail();
        } else {
            Log.e(TAG, "初始化数据失败");
        }
    }

    public String getUserHeadImage() {
        return userHeadImage;
    }

    public String getUserName() {
        return userName;
    }

    public String getUserEmail() {
        return userEmail;
    }
}
