package com.example.zhuhaipeng.babypainter.UserInfo;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.ContentUris;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.zhuhaipeng.babypainter.AllBmobClass.MyUser;
import com.example.zhuhaipeng.babypainter.MainFunctionModules.MainActivityShow;
import com.example.zhuhaipeng.babypainter.R;
import com.example.zhuhaipeng.babypainter.util.Util;
import com.example.zhuhaipeng.babypainter.util.UtilForPicture;


import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.BmobUser;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FetchUserInfoListener;
import cn.bmob.v3.listener.FindListener;

public class UserInfo extends AppCompatActivity  {
    private TextView toProductTree;
    private ImageView userImage;
    private TextView userName;
    private TextView userAge;
    private TextView userSex;
    private TextView userGrade;
    private TextView userEmail;
    private Button userSetting;


    private static final String TAG = "UserInfo";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info);
        initControl();
        toProductTree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UserInfo.this, ProductionTree.class);
                startActivity(intent);
            }
        });
        userSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UserInfo.this, UserDataEditing.class);
                startActivity(intent);
            }
        });
        MyUser user = BmobUser.getCurrentUser(MyUser.class);
        userName.setText(user.getUsername());
        userSex.setText(user.getSex());
        userAge.setText(user.getAge());
        userGrade.setText(user.getGrade());
        Glide.with(UserInfo.this).
                load(user.getImage()).error(R.drawable.dog).
                placeholder(R.drawable.dog).into(userImage);
    }

    public void ReturnToUpperInterface(View view){
        finish();
    }

    /**
     * 初始化控件
     */
    private void initControl(){
        toProductTree = findViewById(R.id.tv_goToProductTree);
        userImage = findViewById(R.id.touxiang);
        Log.e(TAG, "initControl: "+userImage );
        userName = findViewById(R.id.tv_nicheng);
        userAge = findViewById(R.id.tv_nianling);
        userGrade = findViewById(R.id.tv_niaji);
        userSex = findViewById(R.id.tv_xingbie);
        userSetting = findViewById(R.id.btn_setting_user_info);
    }

    /**
     *  应用返回该界面时，更细用户信息
     */
    @Override
    protected void onRestart() {
        super.onRestart();
        fetchUserInfo();
        Log.e(TAG, "onRestart: " + "更新用户信息");
        MyUser user = BmobUser.getCurrentUser(MyUser.class);
        userName.setText(user.getUsername());
        userSex.setText(user.getSex());
        userAge.setText(user.getAge());
        userGrade.setText(user.getGrade());
        Glide.with(UserInfo.this).
                load(user.getImage()).error(R.drawable.dog).
                placeholder(R.drawable.dog).into(userImage);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e(TAG, "onResume: " + "更新用户信息");
//        fetchUserInfo();
        MyUser user = BmobUser.getCurrentUser(MyUser.class);
        userName.setText(user.getUsername());
        userSex.setText(user.getSex());
        userAge.setText(user.getAge());
        userGrade.setText(user.getGrade());
        Glide.with(UserInfo.this).
                load(user.getImage()).error(R.drawable.dog).
                placeholder(R.drawable.dog).into(userImage);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.e(TAG, "onStart: " + "更新用户信息");
        fetchUserInfo();
        MyUser user = BmobUser.getCurrentUser(MyUser.class);
        userName.setText(user.getUsername());
        userSex.setText(user.getSex());
        userAge.setText(user.getAge());
        userGrade.setText(user.getGrade());
        Glide.with(UserInfo.this).load(user.getImage())
                .error(R.drawable.dog)
                .placeholder(R.drawable.dog)
                .into(userImage);
    }
    /**
     * 查询用户表
     */
    private void queryUser() {
        BmobQuery<MyUser> bmobQuery = new BmobQuery<>();
        bmobQuery.findObjects(new FindListener<MyUser>() {
            @Override
            public void done(List<MyUser> object, BmobException e) {
                if (e == null) {
                    Toast.makeText(UserInfo.this, "查询成功", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(UserInfo.this, "查询失败：" + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    /**
     * 同步控制台数据到缓存中
     */
    private void fetchUserInfo() {
        BmobUser.fetchUserInfo(new FetchUserInfoListener<BmobUser>() {
            @Override
            public void done(BmobUser user, BmobException e) {
                if (e == null) {
                    final MyUser myUser = BmobUser.getCurrentUser(MyUser.class);
                    Log.e(TAG, "更新本地缓存信息成功");
                } else {
                    Log.e("error",e.getMessage());
                }
            }
        });
    }


}
