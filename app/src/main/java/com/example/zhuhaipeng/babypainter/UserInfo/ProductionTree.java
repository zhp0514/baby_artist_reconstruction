package com.example.zhuhaipeng.babypainter.UserInfo;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;


import com.bumptech.glide.Glide;
import com.example.zhuhaipeng.babypainter.AllBmobClass.MyUser;
import com.example.zhuhaipeng.babypainter.R;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.BmobUser;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;
import de.hdodenhof.circleimageview.CircleImageView;

public class ProductionTree extends AppCompatActivity {
    private static final String TAG = "ProductionTree";
    Button toDate;
    Button toMoreProduct;
    Button goBack;
    private int[] idList = new int[9];
    private static List<String> productTree = new ArrayList<>();
    private CircleImageView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_production_tree);
        // 初始化控件id
        initIdList();
        // 先服务器查询画过的图片
        init();
        Intent intent = getIntent();
        final String wheres = intent.getStringExtra("where");
        Log.e(TAG, "onCreate: "+ wheres );
        toDate = findViewById(R.id.btn_ziliao);
        toDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if("Main".equals(wheres)){
                    Log.e(TAG, "从MainActivityShow中进入的");
                    Intent intent = new Intent(ProductionTree.this, UserInfo.class);
                    startActivity(intent);
                    finish();
                }else{
                    finish();
                }
            }
        });

        toMoreProduct = findViewById(R.id.btn_gengduo);
        toMoreProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProductionTree.this, MoreProduct.class);
                startActivity(intent);
            }
        });

        goBack = findViewById(R.id.btn_fanhui);
        goBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    /**
     * 初始化作品树
     */
    private void initTree(){
        Log.e(TAG, "productTree--------------->" +productTree.size());
        if(productTree.size() <= 9){ //判断我们的作品数量是否超过了树上可挂作品数
            for(int i = 0; i < productTree.size();i++){
                imageView = findViewById(idList[i]);
                Glide.with(this).load(productTree.get(i)).into(imageView);
            }
        }
    }

    /**
     *  向后台服务器查询画过的图片
     */
    private void init(){
        final MyUser myUser = BmobUser.getCurrentUser(MyUser.class);
        BmobQuery<MyUser> userBmobQuery = new BmobQuery<>();
        userBmobQuery.findObjects(new FindListener<MyUser>() {
            @Override
            public void done(List<MyUser> object, BmobException e) {
                if(e==null){
                    Log.e("bmob","查询成功：共" + object.size() + "条数据。");
                    for(MyUser user: object){
                        if(user.getObjectId().equals(myUser.getObjectId())){
                            productTree = user.getPainted();
                        }
                    }
                }else{
                    Log.e("bmob","失败："+e.getMessage());
                }
                try{
                    initTree();
                }catch (Exception e1){
                    Log.e(TAG, "InitTree出错"+e.getMessage());
                }

            }
        });

    }
    public Bitmap stringtoBitmap(String string) {
        //将字符串转换成Bitmap类型
        Bitmap bitmap = null;
        try {
            byte[] bitmapArray;
            bitmapArray = Base64.decode(string, Base64.DEFAULT);
            bitmap = BitmapFactory.decodeByteArray(bitmapArray, 0, bitmapArray.length);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    /**
     * 初始化控件id
     */
    private void initIdList(){
        idList[0] = R.id.RIV_zuopin1;
        idList[1] = R.id.RIV_zuopin2;
        idList[2] = R.id.RIV_zuopin3;
        idList[3] = R.id.RIV_zuopin4;
        idList[4] = R.id.RIV_zuopin5;
        idList[5] = R.id.RIV_zuopin6;
        idList[6] = R.id.RIV_zuopin7;
        idList[7] = R.id.RIV_zuopin8;
        idList[8] = R.id.RIV_zuopin9;
    }
}
