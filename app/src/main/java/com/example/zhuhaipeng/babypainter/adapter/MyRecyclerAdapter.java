package com.example.zhuhaipeng.babypainter.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.zhuhaipeng.babypainter.AllBmobClass.MyUser;
import com.example.zhuhaipeng.babypainter.AllBmobClass.Post;
import com.example.zhuhaipeng.babypainter.Community.PostMessage;
import com.example.zhuhaipeng.babypainter.R;


import java.util.ArrayList;
import java.util.List;

import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.BmobUser;
import cn.bmob.v3.datatype.BmobPointer;
import cn.bmob.v3.datatype.BmobRelation;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;
import cn.bmob.v3.listener.QueryListener;
import cn.bmob.v3.listener.UpdateListener;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * RecyclerView的适配器
 */
public class MyRecyclerAdapter extends RecyclerView.Adapter<MyRecyclerAdapter.MyViewHodler> {

    private  Context context;
    private ArrayList<Post> data;
    private View itemView;
    private static final String TAG = "MyRecyclerAdapter";
    private View parentView;
    private ImageView mBg;
    private  FrameLayout mParent;
    public static boolean flag = false;
    class MyViewHodler extends  RecyclerView.ViewHolder{
        private TextView tv_name;
        private CircleImageView circleImageView;
        private GridView gridView;
        private TextView article;
        private RelativeLayout linearLayout;
        private ImageView dian_zan;
        private TextView browsedNumber;
        private ImageView comment;

        public MyViewHodler(@NonNull View itemView) {
            super(itemView);
            linearLayout = (RelativeLayout)itemView.findViewById(R.id.constraintLayout2);
            tv_name =(TextView) itemView.findViewById(R.id.tv_name);
            article =(TextView) itemView.findViewById(R.id.article);
            gridView = (GridView) itemView.findViewById(R.id.Clickgradview);
            circleImageView = (CircleImageView) itemView.findViewById(R.id.circleImageView);
            dian_zan = (ImageView) itemView.findViewById(R.id.dian_zan);
            browsedNumber = (TextView) itemView.findViewById(R.id.liu_lan_ci_shu);
            comment = (ImageView) itemView.findViewById(R.id.comment);
        }
    }
    @NonNull
    /**
     * 相当于ListView中getView方法中创建View和ViewHodler
     */
    @Override
    public MyViewHodler onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        itemView = LayoutInflater.from(context).inflate(R.layout.item_recyclerview,viewGroup,false);
        mParent = (FrameLayout)parentView.findViewById(R.id.parent_find);
        mBg = (ImageView)parentView.findViewById(R.id.bg_find);

        final MyViewHodler hodler = new MyViewHodler(itemView);

        // 点击帖子，打开帖子页面
        hodler.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos = hodler.getAdapterPosition();
                Post item = data.get(pos);
                String s = "1"; //代表点击了帖子
                Intent intent = new Intent(context, PostMessage.class);
                intent.putExtra("Post",item);
                intent.putExtra("flag",s);
                context.startActivity(intent);
            }
        });
        // 点击点赞图标
        hodler.dian_zan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                query(hodler);
            }
        });
        hodler.comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos = hodler.getAdapterPosition();
                Post item = data.get(pos);
                String s = "2";//代表点击了评论
                Intent intent = new Intent(context,PostMessage.class);
                intent.putExtra("Post",item);
                intent.putExtra("flag",s);
                context.startActivity(intent);
            }
        });
        return hodler;
    }

    public MyRecyclerAdapter(Context context, ArrayList<Post>data,View view) {
        this.context = context;
        this.data = data;
        this.parentView = view;
    }

    public void clear(){
        data.clear();
        BmobQuery<Post> query = new BmobQuery<Post>();
        //查询author不为空的数据  不写下面的语句就表示全部查询
        //query.addWhereNotEqualTo("objectId","");
        //返回10条数据
        query.include("author");
        query.setLimit(500);
        //执行查询方法
        query.findObjects(new FindListener<Post>() {
            @Override
            public void done(List<Post> list1, BmobException e) {
                if(e==null){
                    int i = 0;
                    for(Post post:list1){
                        data.add(post);
                    }
                }else{
                    Log.e(TAG, "done:xxxxxxx " );
                }
            }
        });
    }

    private void queryAgain(final Post post, final MyViewHodler hodler){
        BmobQuery<Post> query = new BmobQuery<Post>();
        query.getObject(post.getObjectId(), new QueryListener<Post>() {
            @Override
            public void done(Post object, BmobException e) {
                if(e==null){
                    Log.e(TAG, "浏览次数 "+data.get(hodler.getAdapterPosition()).getBrowseNumber());
                    hodler.browsedNumber.setText(String.valueOf(object.getBrowseNumber()));
                }else{
                    Log.i("bmob","失败："+e.getMessage()+","+e.getErrorCode());
                }
            }

        });
    }
    // 查询
    private void query(final MyViewHodler hodler){
        // 获得当前登陆的用户
        final MyUser myUser = BmobUser.getCurrentUser(MyUser.class);
        BmobQuery<MyUser> bmobQuery = new BmobQuery<MyUser>();
        Post post = data.get(hodler.getAdapterPosition());
        //likes是Post表中的字段，用来存储所有喜欢该帖子的用户
        bmobQuery.addWhereRelatedTo("likes",new BmobPointer(post));
        // 查询
        bmobQuery.findObjects(new FindListener<MyUser>() {
            @Override
            public void done(List<MyUser> list, BmobException e) {
                if(e==null){
//                    Log.e(TAG, " 喜欢该帖子的人数： "+list.size() );
                    // 判断当前喜欢该帖子的用户人数，如果大于零，用户点赞后进行相关判断
                    if(list.size() > 0){
                        int count = 0;
                        for(MyUser user : list){
                            // 如果已经点过赞，再次点击后赞会取消，浏览次数减一
                            if(user.getObjectId().equals(myUser.getObjectId())){
                                // 设置点赞图片的状态
                                Resources resources = context.getResources();
                                Drawable drawable = resources.getDrawable(R.drawable.heart);
                                hodler.dian_zan.setImageDrawable(drawable);

                                MyUser myUser1 = BmobUser.getCurrentUser(MyUser.class);
                                String id = data.get(hodler.getAdapterPosition()).getObjectId();
                                Post post = new Post();
                                post.setObjectId(id);
                                BmobRelation relation = new BmobRelation();
                                relation.remove(myUser1);
                                post.setLikes(relation);
                                post.increment("browseNumber",-1);
                                post.update(new UpdateListener() {
                                    @Override
                                    public void done(BmobException e) {
                                        if(e==null){
                                            Log.e("bmob","关联关系删除成功");
                                            queryAgain(data.get(hodler.getAdapterPosition()),hodler);
                                        }else{
                                            Log.e("bmob","失败："+e.getMessage());
                                        }
                                    }
                                });

                                break;
                            }else{
                                ++count;
                            }
                        }
//                        // 说明当前用户没有给该帖子点过赞
                        if(count >= list.size()){
                            // 设置点赞图片的状态
                            Resources resources = context.getResources();
                            Drawable drawable = resources.getDrawable(R.drawable.red_heart);
                            hodler.dian_zan.setImageDrawable(drawable);


                            String id = data.get(hodler.getAdapterPosition()).getObjectId();
                            Post post = new Post();
                            //将当前用户添加到Post表中的likes字段值中，表明当前用户喜欢该帖子
                            BmobRelation relation = new BmobRelation();
                            //将当前用户添加到多对多关联中
                            relation.add(myUser);
                            //多对多关联指向`post`的`likes`字段
                            post.setLikes(relation);
                            post.increment("browseNumber");
                            post.update(id,new UpdateListener() {
                                @Override
                                public void done(BmobException e) {
                                    if(e==null){
                                        Log.e(TAG, "done: 更新次数成功");
                                        queryAgain(data.get(hodler.getAdapterPosition()),hodler);
                                    }else{
                                        Log.e(TAG, "done: 更新次数失败" );
                                    }
                                }
                            });
                            count = 0;
                        }
                    } else {  //当前帖子无用户点赞，现用户点赞的情况
                        // 设置点赞图片的状态
                        Resources resources = context.getResources();
                        Drawable drawable = resources.getDrawable(R.drawable.red_heart);
                        hodler.dian_zan.setImageDrawable(drawable);

                        String id = data.get(hodler.getAdapterPosition()).getObjectId();
                        Post post = new Post();
                        //将当前用户添加到Post表中的likes字段值中，表明当前用户喜欢该帖子
                        BmobRelation relation = new BmobRelation();
                        //将当前用户添加到多对多关联中
                        relation.add(myUser);
                        //多对多关联指向`post`的`likes`字段
                        post.setLikes(relation);
                        post.increment("browseNumber");
                        post.update(id,new UpdateListener() {
                            @Override
                            public void done(BmobException e) {
                                if(e==null){
                                    Log.e(TAG, "done: 无点赞时更新次数成功");
                                    queryAgain(data.get(hodler.getAdapterPosition()),hodler);
                                }else{
                                    Log.e(TAG, "done: 无点赞时更新次数失败" );
                                }
                            }
                        });
                    }

                }
            }
        });

    }

    // 防止下滑后数据错乱
    @Override
    public int getItemViewType(int position) {
        return position;
    }

    /**相当于getView绑定数据部分的代码
     * 数据和View绑定
     * @param myViewHodler
     * @param i
     */
    @Override
    public void onBindViewHolder(@NonNull final MyViewHodler myViewHodler, int i) {
       //根据位置得到对应数据
        Post item = data.get(i);
        final int pos = i;
        myViewHodler.tv_name.setText(item.getAuthor().getUsername()); // 获取并设置用户名

         if(item.getAuthor().getImage() != null){ // 使用用户自己的头像
             Glide.with(context).load(item.getAuthor().getImage()).into( myViewHodler.circleImageView);
         }else{ // 使用默认头像
             Resources resources = context.getResources();
             Drawable drawable = resources.getDrawable(R.drawable.nav_icon);
             myViewHodler.circleImageView.setImageDrawable(drawable);
         }

        myViewHodler.article.setText(item.getContent()); // 获取并设置用户帖子内容

        myViewHodler.browsedNumber.setText(String.valueOf(item.getBrowseNumber())); // 获取并设置帖子浏览次数

        // 获得当前登陆的用户
        final MyUser myUser = BmobUser.getCurrentUser(MyUser.class);
        BmobQuery<MyUser> bmobQuery = new BmobQuery<MyUser>();
        Post post = data.get(i);
        //likes是Post表中的字段，用来存储所有喜欢该帖子的用户
        bmobQuery.addWhereRelatedTo("likes",new BmobPointer(post));
        // 查询
        bmobQuery.findObjects(new FindListener<MyUser>() {
            @Override
            public void done(List<MyUser> list, BmobException e) {
                if (e == null) {
//                    Log.e(TAG, "onBindViewHolder: 查询个数" + list.size());
                    // 判断当前喜欢该帖子的用户人数，如果大于零，用户点赞后进行相关判断
                    if (list.size() > 0) {
                        for (MyUser user : list) {
                            // 如果已经点过赞，则显示红心
                            if (user.getObjectId().equals(myUser.getObjectId())) {
                                // 设置点赞图片的状态
                                Resources resources = context.getResources();
                                Drawable drawable = resources.getDrawable(R.drawable.red_heart);
                                myViewHodler.dian_zan.setImageDrawable(drawable);
                                break;
                            }else{
                                Resources resources = context.getResources();
                                Drawable drawable = resources.getDrawable(R.drawable.heart);
                                myViewHodler.dian_zan.setImageDrawable(drawable);
                            }
                        }
                    }else{  //没有点赞
                        Resources resources = context.getResources();
                        Drawable drawable = resources.getDrawable(R.drawable.heart);
                        myViewHodler.dian_zan.setImageDrawable(drawable);
                    }
                }
            }
        });

        //判断用户发的帖子是否带照片
         if(data.get(i).getPicture() != null){
//             Log.e(TAG, "用户发表的帖子带有图片" );
             if(data.get(i).getPicture().size()>0){
                 GradViewAdapter gradViewAdapter = new GradViewAdapter(context, data.get(i).getPicture());
                 myViewHodler.gridView.setAdapter(gradViewAdapter);
                 //添加消息处理
                 myViewHodler.gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                     @Override
                     public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                         Log.e(TAG, "点击了gradView");
                         flag = true;
                         mBg.setVisibility(View.VISIBLE);
                         mParent.setVisibility(View.VISIBLE);
                         Glide.with(view).load(data.get(pos).getPicture().get(position)).into(mBg);
                     }
                 });

//                  点击显示后的大图，将fragment隐藏
                 mBg.setOnClickListener(new View.OnClickListener() {
                     @Override
                     public void onClick(View v) {
                         Log.e(TAG, "Mylog: 点击了图片" );
                         mParent.setVisibility(View.GONE);
                         flag = false;
                     }
                 });
             }
         }

    }


    /**
     * 得到总条数
     * @return
     */
    @Override
    public int getItemCount() {
        return data.size();
    }


}

