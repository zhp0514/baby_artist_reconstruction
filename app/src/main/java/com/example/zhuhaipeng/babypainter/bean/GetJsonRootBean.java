package com.example.zhuhaipeng.babypainter.bean; /**
 * Copyright 2019 bejson.com
 */
import android.content.Intent;

import java.util.List;

/**
 * Auto-generated: 2019-04-19 12:52:53
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class GetJsonRootBean {

    private Intent intent;
    private List<Results> results;
    public void setIntent(Intent intent) {
        this.intent = intent;
    }
    public Intent getIntent() {
        return intent;
    }

    public void setResults(List<Results> results) {
        this.results = results;
    }
    public List<Results> getResults() {
        return results;
    }

}