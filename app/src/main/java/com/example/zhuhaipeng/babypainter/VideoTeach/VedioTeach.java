package com.example.zhuhaipeng.babypainter.VideoTeach;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.VideoView;

import com.example.zhuhaipeng.babypainter.R;


public class VedioTeach extends AppCompatActivity {

    private VideoView videoView;
    private ImageView back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fruit_vedio_teach);

        back = (ImageView) findViewById(R.id.backInFruit);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        videoView = (VideoView) findViewById(R.id.videoViewForFruit);
        videoView.setVideoPath("http://bmob-cdn-19476.b0.upaiyun.com/2018/11/08/91cd34884002299c80eccbcd45782c5e.mp4");
        //创建MediaController对象
        MediaController mediaController = new MediaController(this);

        //VideoView与MediaController建立关联
        videoView.setMediaController(mediaController);

        //让VideoView获取焦点
        videoView.requestFocus();
    }
    @Override
    public void onBackPressed() {
        //此处写退向后台的处理
        this.finish();
    }
}
