package com.example.zhuhaipeng.babypainter.MainFunctionModules;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.zhuhaipeng.babypainter.R;
import com.example.zhuhaipeng.babypainter.util.TTSUtils;

public class Setting extends AppCompatActivity implements View.OnClickListener {
    private Button xiaoxin;
    private Button nannan;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        xiaoxin = (Button)findViewById(R.id.xiaoxin);
        nannan = (Button)findViewById(R.id.nannan);

        xiaoxin.setOnClickListener(this);
        nannan.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.xiaoxin:
                TTSUtils.chooseSpeaking = 15;
                Toast.makeText(Setting.this,"选择小新为发音人",Toast.LENGTH_SHORT).show();
                break;
            case R.id.nannan:
                TTSUtils.chooseSpeaking = 16;
                Toast.makeText(Setting.this,"选择楠楠为发音人",Toast.LENGTH_SHORT).show();
                break;
                default:
                    break;
        }
    }
}
