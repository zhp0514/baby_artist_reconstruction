package com.example.zhuhaipeng.babypainter.FragmentModules;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.zhuhaipeng.babypainter.AllBmobClass.Post;
import com.example.zhuhaipeng.babypainter.Community.PublishArticle;
import com.example.zhuhaipeng.babypainter.MainFunctionModules.MainActivityShow;
import com.example.zhuhaipeng.babypainter.R;
import com.example.zhuhaipeng.babypainter.UserInfo.MoreProduct;
import com.example.zhuhaipeng.babypainter.VideoPage.Cartoon;
import com.example.zhuhaipeng.babypainter.VideoPage.ListenMusic;
import com.example.zhuhaipeng.babypainter.VideoTeach.VideoTeaching;
import com.example.zhuhaipeng.babypainter.adapter.GlideImageLoader;
import com.example.zhuhaipeng.babypainter.adapter.MyRecyclerAdapter;
import com.example.zhuhaipeng.babypainter.view.MyScrollView;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.youth.banner.Banner;
import com.youth.banner.BannerConfig;
import com.youth.banner.Transformer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.datatype.BmobDate;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;

/**
 * @author gg_boy
 * @version $Rev$
 * @des ${发现界面}
 * @updateAuther $Auther$
 * @updateDes ${TODO}
 */
public class FindFragment extends Fragment implements View.OnClickListener{
    private Banner banner;
    private static final String TAG = "FindFragment";
    //设置图片资源:url或本地资源
    List<String> images = new ArrayList<>();
    private RecyclerView recyclerView;
    //建立数据
    private  ArrayList<Post> data = new ArrayList<>();
    private MyRecyclerAdapter adapter;
    private int start;
    private static View view = null;
    private ImageView publishArticle = null;

    // 用于判断向服务器请求的结果，，如果不行，换一种请求方式
    private static boolean requestFlag = true;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if(view == null){ // 第一次打开
            view = inflater.inflate(R.layout.find_fragment, container, false);
            initCotrol();
            initView();
            getDataFromServer();
            return view;
        }else{ // 不是第一次打开就不再初始化数据
            return view;
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.publish_find:  // 点击发表帖子
                Intent intent = new Intent(v.getContext(), PublishArticle.class);
                startActivity(intent);
                break;
            case R.id.linerLayout1:
                Intent intent1 = new Intent(v.getContext(), Cartoon.class);
                startActivity(intent1);
                break;
            case R.id.linerLayout2:
                Intent intent2 = new Intent(v.getContext(), ListenMusic.class);
                startActivity(intent2);
                break;
            case R.id.linerLayout3:
                Intent intent3 = new Intent(v.getContext(), VideoTeaching.class);
                startActivity(intent3);
                break;
            case R.id.linerLayout4:
                break;
            case R.id.linerLayout5:
                break;
            case R.id.linerLayout6:
                break;
            default:
                break;
        }
    }

    private static class Holder{
        private final static FindFragment sFindFragment = new FindFragment();
    }
    public static FindFragment getInstance(){
        return Holder.sFindFragment;
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e(TAG, "onResume: "+"findFragment");
        queryPostData();
    }

    protected void initView() {
        // 轮播图中的图片
        images.add("http://bmob-cdn-19476.b0.upaiyun.com/2018/11/15/b77c69d6a7db4c448840ddeaff124c5b.jpg");
        images.add("http://bmob-cdn-19476.b0.upaiyun.com/2018/10/31/2e23819601ad434b996610a2783201dd.jpg");
        banner = (Banner) view.findViewById(R.id.banner);
        // 找到recyclerView控件id
        recyclerView = (RecyclerView) view.findViewById(R.id.rv_find);
        LinearLayoutManager linearLayout = new LinearLayoutManager(view.getContext());
        recyclerView.setLayoutManager(linearLayout);
        recyclerView.setFocusableInTouchMode(false); //为了防止点击item后自动回滚
        // 初始化recyclerView中要显示的数据
        initData();
    }
    /**
     * 初始化数据
     */
    private void initData() {
        BmobQuery<Post> query = new BmobQuery<>();
        //查询author不为空的数据  不写下面的语句就表示全部查询
        //query.addWhereNotEqualTo("objectId","");
        //返回50条数据
        query.include("author");
        query.setLimit(50);
        //执行查询方法
        query.findObjects(new FindListener<Post>() {
            @Override
            public void done(List<Post> list1, BmobException e) {
                if(e==null){
                    Log.e(TAG, "共: "+list1.size()+"条数据" );
                    if(data.size() == 0){
                        data.addAll(list1);
                        start = data.size();
                        Collections.reverse(data); // 反转数据
                        //实例化一个适配器，并将这个适配器对象传递进setAdapter中
                        FrameLayout frameLayout = (FrameLayout)view.findViewById(R.id.parent_find) ;
                        adapter = new MyRecyclerAdapter(view.getContext(),data,frameLayout);
                        recyclerView.setAdapter(adapter);
                    }
                }else{
                    Log.e(TAG, "查询失败" + e.getCause() + ""+e.getLocalizedMessage());
                    Toast.makeText(view.getContext(), "与服务器连接失败！！", Toast.LENGTH_LONG).show();
                }
            }
        });
    }
    public Banner getBanner(){
        return banner;
    }

    public int getLayoutId() {
        return R.layout.find_fragment;
    }

    protected void getDataFromServer() {
        banner = (Banner) getBanner();
        //设置样式,默认为:Banner.NOT_INDICATOR(不显示指示器和标题)
        //可选样式如下:
        //1. Banner.CIRCLE_INDICATOR    显示圆形指示器
        //2. Banner.NUM_INDICATOR   显示数字指示器
        //3. Banner.NUM_INDICATOR_TITLE 显示数字指示器和标题
        //4. Banner.CIRCLE_INDICATOR_TITLE  显示圆形指示器和标题

        banner.setBannerStyle(BannerConfig.CIRCLE_INDICATOR);
        //设置图片加载器
        banner.setImageLoader(new GlideImageLoader());
        //设置图片集合
        banner.setImages(images);
        //设置banner动画效果
        banner.setBannerAnimation(Transformer.DepthPage);
        //设置标题集合（当banner样式有显示title时）
        //        banner.setBannerTitles(titles);
        //设置自动轮播，默认为true
        banner.isAutoPlay(true);
        //设置轮播时间
        banner.setDelayTime(1500);
        //设置指示器位置（当banner模式中有指示器时）
        banner.setIndicatorGravity(BannerConfig.CENTER);
        //banner设置方法全部调用完毕时最后调用
        banner.start();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    public void initCotrol(){
        // 发表说说按钮
        publishArticle = view.findViewById(R.id.publish_find);
        publishArticle.setOnClickListener(this);
        // 中间六个功能选择的控件
        LinearLayout linearLayout1 = view.findViewById(R.id.linerLayout1);
        linearLayout1.setOnClickListener(this);
        LinearLayout linearLayout2 = view.findViewById(R.id.linerLayout2);
        linearLayout2.setOnClickListener(this);
        LinearLayout linearLayout3 = view.findViewById(R.id.linerLayout3);
        linearLayout3.setOnClickListener(this);
        LinearLayout linearLayout4 = view.findViewById(R.id.linerLayout4);
        linearLayout4.setOnClickListener(this);
        LinearLayout linearLayout5 = view.findViewById(R.id.linerLayout5);
        linearLayout5.setOnClickListener(this);
        LinearLayout linearLayout6 = view.findViewById(R.id.linerLayout6);
        linearLayout6.setOnClickListener(this);

        // 下拉刷新控件
        RefreshLayout refreshLayout = (RefreshLayout)view.findViewById(R.id.refreshLayout_find);
//        refreshLayout.autoRefresh(1000);
        refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                queryPostData();
                refreshlayout.finishRefresh(2000);
            }
        });
    }

    private void queryPostData() {
        if(requestFlag){
            BmobQuery<Post> query = new BmobQuery<>();
            query.include("author");
            //        query.setLimit(30);
            //执行查询方法
            query.findObjects(new FindListener<Post>() {
                @Override
                public void done(List<Post> list1, BmobException e) {
                    if(e==null){
                        Log.e(TAG, "刷新成功");
                        int i = 0;
                        if(list1.size() < data.size()){
                            data.clear();
                            data.addAll(list1);
                        }else{
                            for(Post post:list1){
                                if(i >=  start){
                                    data.add(0,post);
                                }
                                i++;
                            }
                        }
                        // Collections.reverse(data); // 反转数据
                        //实例化一个适配器，并将这个适配器对象传递进setAdapter中
                        FrameLayout frameLayout = (FrameLayout)view.findViewById(R.id.parent_find) ;
                        adapter = new MyRecyclerAdapter(view.getContext(),data,frameLayout);
                        recyclerView.setAdapter(adapter);
                        start = data.size();
                    }else{
                        Toast.makeText(view.getContext(), "与服务器连接失败！！", Toast.LENGTH_LONG).show();
                        Log.e(TAG, "查询失败 "+e.getMessage()+""+e.getErrorCode());
                        requestFlag = false;
                    }
                }
            });
        }else{
            // 设置查询的最晚时间
            String createdAt = "2019-01-01 13:49:20";
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date createdAtDate = null;
            try {
                createdAtDate = sdf.parse(createdAt);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            BmobDate bmobCreatedAtDate = new BmobDate(createdAtDate);

            BmobQuery<Post> query = new BmobQuery<>();
            query.include("author");
            query.addWhereGreaterThan("createdAt", bmobCreatedAtDate);
            //        query.setLimit(30);
            //执行查询方法
            query.findObjects(new FindListener<Post>() {
                @Override
                public void done(List<Post> list1, BmobException e) {
                    if(e==null){
                        Log.e(TAG, "刷新成功");
                        int i = 0;
                        if(list1.size() < data.size()){
                            data.clear();
                            data.addAll(list1);
                        }else{
                            for(Post post:list1){
                                if(i >=  start){
                                    data.add(0,post);
                                }
                                i++;
                            }
                        }
                        // Collections.reverse(data); // 反转数据
                        //实例化一个适配器，并将这个适配器对象传递进setAdapter中
                        FrameLayout frameLayout = (FrameLayout)view.findViewById(R.id.parent_find) ;
                        adapter = new MyRecyclerAdapter(view.getContext(),data,frameLayout);
                        recyclerView.setAdapter(adapter);
                        start = data.size();
                    }else{
                        Toast.makeText(view.getContext(), "与服务器连接失败！！", Toast.LENGTH_LONG).show();
                        Log.e(TAG, "查询失败 "+e.getMessage()+""+e.getErrorCode());
                    }
                }
            });
        }

    }
}
