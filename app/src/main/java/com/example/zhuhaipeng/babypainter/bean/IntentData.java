package com.example.zhuhaipeng.babypainter.bean; /**
 * Copyright 2019 bejson.com
 */

/**
 * Auto-generated: 2019-04-19 12:52:53
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class IntentData {

    private String actionName;
    private int code;
    private String intentName;
    public void setActionName(String actionName) {
        this.actionName = actionName;
    }
    public String getActionName() {
        return actionName;
    }

    public void setCode(int code) {
        this.code = code;
    }
    public int getCode() {
        return code;
    }

    public void setIntentName(String intentName) {
        this.intentName = intentName;
    }
    public String getIntentName() {
        return intentName;
    }

}