package com.example.zhuhaipeng.babypainter.MainFunctionModules;

import android.content.Intent;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.widget.Toast;

import com.example.zhuhaipeng.babypainter.AllBmobClass.MyUser;
import cn.bmob.v3.Bmob;
import cn.bmob.v3.BmobUser;
import pub.devrel.easypermissions.EasyPermissions;

public class EntranceActivity extends AppCompatActivity {

    private Handler handler = new Handler();
    private static long exitTime = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //第一：默认初始化
        Bmob.initialize(this, "8c6065e6d0e046992cd72762e157fce2");
        //展示2s后前往登陆和注册选择页面
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                gotoLogin();
            }
        }, 1000);
    }

    /**
     * 请求权限
     * @param requestCode 请求码
     * @param permissions 要请求的权限
     * @param grantResults 请求结果
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode,permissions,grantResults,this);
    }

    /**
     * 前往注册、登录主页
     */
    private void gotoLogin() {

        MyUser bmobUser = BmobUser.getCurrentUser(MyUser.class);
        if(bmobUser != null){
            // 允许用户使用应用
            Intent intent = new Intent(EntranceActivity.this,MainActivityShow.class);
            startActivity(intent);
            finish();
        }else{
            //缓存用户对象为空时， 可打开用户登陆和注册选择界面
            Intent intent = new Intent(EntranceActivity.this, LogInOrRegisterActivity.class);
            startActivity(intent);
            finish();
        }
        //取消界面跳转时的动画，使启动页的logo图片与注册、登录主页的logo图片完美衔接
        overridePendingTransition(0, 0);
    }

    /**
     * 屏蔽物理返回键
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (System.currentTimeMillis() - exitTime > 2000) {
                Toast.makeText(EntranceActivity.this, "再按一次退出程序", Toast.LENGTH_SHORT).show();
                exitTime = System.currentTimeMillis();
            } else{
                moveTaskToBack(true);
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onDestroy() {
        if (handler != null) {
            //If token is null, all callbacks and messages will be removed.
            handler.removeCallbacksAndMessages(null);
        }
        super.onDestroy();
    }
}
