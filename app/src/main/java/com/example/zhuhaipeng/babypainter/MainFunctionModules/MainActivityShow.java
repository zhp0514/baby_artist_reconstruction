package com.example.zhuhaipeng.babypainter.MainFunctionModules;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.zhuhaipeng.babypainter.AllBmobClass.MyUser;
import com.example.zhuhaipeng.babypainter.FragmentModules.CreateFragment;
import com.example.zhuhaipeng.babypainter.FragmentModules.FindFragment;
import com.example.zhuhaipeng.babypainter.FragmentModules.ScrawlFragment;
import com.example.zhuhaipeng.babypainter.R;
import com.example.zhuhaipeng.babypainter.UserInfo.ProductionTree;
import com.example.zhuhaipeng.babypainter.UserInfo.UserInfo;
import com.example.zhuhaipeng.babypainter.util.AuthService;
import com.example.zhuhaipeng.babypainter.util.InitUserInfo;
import com.example.zhuhaipeng.babypainter.util.TTSUtils;
import com.example.zhuhaipeng.babypainter.util.UtilForPicture;
import com.example.zhuhaipeng.babypainter.util.Voice;
import com.google.gson.Gson;
import com.iflytek.cloud.ErrorCode;
import com.iflytek.cloud.InitListener;
import com.iflytek.cloud.RecognizerResult;
import com.iflytek.cloud.SpeechConstant;
import com.iflytek.cloud.SpeechError;
import com.iflytek.cloud.SpeechRecognizer;
import com.iflytek.cloud.SpeechUtility;
import com.iflytek.cloud.ui.RecognizerDialog;
import com.iflytek.cloud.ui.RecognizerDialogListener;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Random;

import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.BmobUser;
import cn.bmob.v3.datatype.BmobFile;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;
import cn.bmob.v3.listener.UpdateListener;
import cn.bmob.v3.listener.UploadFileListener;

public class MainActivityShow extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {
    private static final String TAG = "MainActivityShow";
    private static  CreateFragment mCreateFragment = null;
    private static FindFragment mFindFragment = null;
    private static ScrawlFragment mScrawlFragment = null;
    // 用于选择进行识别图像的弹窗
    private Dialog dialog;
    private View inflate;
    private TextView choosePhoto;
    private TextView takePhoto;
    private TextView choosePhoto1;
    private TextView takePhoto1;
    //
    private String article;
    // 侧滑菜单
    private NavigationView navigationView = null;
    //
    public static final int TAKE_PHOTO = 1;//识别图形启动相机标识
    public static final int CHOOSE_PHOTO = 2;//识别图形启动相册标识
    public static final int TAKE_PHOTOForScrawl = 5;//涂鸦区启动相机标识
    public static final int CHOOSE_PHOTOForScrawl = 6;//涂鸦区启动相册标识
    // 相机拍照后图像的uri
    private static Uri imageUri;
    //图片的base64编码的字符串
    private String base64String;
    // 百度图像识别秘钥
    private static String access_token = "";
    // 图像识别的结果
    public static StringBuffer resultCognition = new StringBuffer(" ");
    // 初始化用户信息
    private InitUserInfo initUserInfo = null;
    //
    private TextView hintText = null;
    private ImageView whatYouPaint = null;
    //语音合成
    public static TTSUtils kqwSpeechCompound = null;
    //跳转unity判断
    public static boolean unity = false;
    public static boolean song = false;  //是否选择播放儿歌
    public static boolean story = false; //是否选择听故事
    // 决定跳转到unity中时打开哪个界面
    public static int index;
    // 屏幕宽度
    public static int mScreenHeight;
    public static int mScreenWidth;
    // 决定显示哪个Fragment\
    public static int scene = 0;
    // 识别后显示图片的控件
    private ImageView iv_recognition = null;

    private HashMap<String, String> mIatResults = new LinkedHashMap<String, String>();
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    replaceFragment(mCreateFragment);
                    return true;
                case R.id.navigation_dashboard:
                    replaceFragment(mScrawlFragment);
                    return true;
                case R.id.navigation_notifications:
                    replaceFragment(mFindFragment);
                    return true;
            }
            return false;
        }
    };
    private SpeechRecognizer mIat;
    private boolean requestFromScrawl = false;
    private Dialog dialogForScrawl;
    // 用户识别成功的作品，上传到服务器进行保存，用于作品展示
    private static String uploadPicture = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SpeechUtility.createUtility(MainActivityShow.this, SpeechConstant.APPID + "=5bc2fced");
        setContentView(R.layout.activity_main_show);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // 获得access_token（百度图像识别api)
        getAccessToken();
        // 初始化Fragment
        initFragment();
        // 实例化用户信息类
        initUserInfo = InitUserInfo.getInstance();
        initUserInfo.initData();
        replaceFragment(mCreateFragment);
        // 底部导航栏
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        // 侧滑菜单
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        // 获取屏幕大小
        WindowManager manager = this.getWindowManager();
        DisplayMetrics outMetrics = new DisplayMetrics();
        manager.getDefaultDisplay().getMetrics(outMetrics);
        mScreenHeight= outMetrics.heightPixels;
        mScreenWidth = outMetrics.widthPixels;

        // 初始化侧滑菜单用户信息
        InitLateralSpreadsMenu();
    }

    /**
     * 初始化侧滑菜单的用户信息，如头像，用户名，邮箱
     */
    private void InitLateralSpreadsMenu() {
        ImageView userHead = (ImageView)navigationView.getHeaderView(0).findViewById(R.id.iv_user_head);
        TextView userName = (TextView)navigationView.getHeaderView(0).findViewById(R.id.tv_user_name);
        TextView userEmail = (TextView) navigationView.getHeaderView(0).findViewById(R.id.tv_user_email);
        userEmail.setText(initUserInfo.getUserEmail());
        userName.setText(initUserInfo.getUserName());
        Glide.with(this).
                load(initUserInfo.getUserHeadImage()).
                error(R.drawable.dog).
                placeholder(R.drawable.dog).
                into(userHead);
        userHead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(TAG, "点击用户头像" );
                Intent intent = new Intent(MainActivityShow.this, UserInfo.class);
                startActivity(intent);
            }
        });
    }

    /**
     * 获得 access_token
     */
    private void getAccessToken() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                while(access_token.equals("")){
                    access_token = AuthService.getAuth();
                }
            }
        }).start();
    }
    @Override
    protected void onStart() {
        super.onStart();
        //
        kqwSpeechCompound = new TTSUtils(this);

        // 给CreateFragment中的控件设置监听事件
        hintText = mCreateFragment.tv_hint;
        hintText.setOnClickListener(this);
        whatYouPaint = mCreateFragment.whatYouPaint;
        whatYouPaint.setOnClickListener(this);

    }



    @Override
    protected void onResume() {
        super.onResume();
        iv_recognition = mCreateFragment.mImageView;
        iv_recognition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recognition();
            }
        });
        if(scene == 3){
           replaceFragment(mFindFragment);
        }
    }


    /**
     * 点击CreateFragment中画板显示的弹窗
     */
    public void recognition(){
        requestFromScrawl = false;
        dialog = new Dialog(this, R.style.ActionSheetDialogStyle);
        //填充对话框的布局
        inflate = LayoutInflater.from(this).inflate(R.layout.dialog_layout, null);
        //初始化控件
        choosePhoto = (TextView) inflate.findViewById(R.id.choosePhoto);
        takePhoto = (TextView) inflate.findViewById(R.id.takePhoto);
        choosePhoto.setOnClickListener(this);
        takePhoto.setOnClickListener(this);
        //将布局设置给Dialog
        dialog.setContentView(inflate);
        //获取当前Activity所在的窗体
        Window dialogWindow = dialog.getWindow();
        //设置Dialog从窗体底部弹出
        assert dialogWindow != null;
        dialogWindow.setGravity( Gravity.BOTTOM);
        //获得窗体的属性
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        lp.y = 20;//设置Dialog距离底部的距离
        // 将属性设置给窗体
        dialogWindow.setAttributes(lp);
        dialog.show();//显示对话框
    }

    /**
     * 趣味互动的选择弹窗
     */
    public void showDialog(){
        requestFromScrawl = true;
        dialogForScrawl = new Dialog(this, R.style.ActionSheetDialogStyle);
        //填充对话框的布局
        inflate = LayoutInflater.from(this).inflate(R.layout.dialog_layout, null);
        //初始化控件
        choosePhoto1 = (TextView) inflate.findViewById(R.id.choosePhoto);
        takePhoto1 = (TextView) inflate.findViewById(R.id.takePhoto);
        choosePhoto1.setOnClickListener(this);
        takePhoto1.setOnClickListener(this);
        //将布局设置给Dialog
        dialogForScrawl.setContentView(inflate);
        //获取当前Activity所在的窗体
        Window dialogWindow = dialogForScrawl.getWindow();
        //设置Dialog从窗体底部弹出
        assert dialogWindow != null;
        dialogWindow.setGravity( Gravity.BOTTOM);
        //获得窗体的属性
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        lp.y = 20;//设置Dialog距离底部的距离
        // 将属性设置给窗体
        dialogWindow.setAttributes(lp);
        dialogForScrawl.show();//显示对话框
    }
    /**
     * 初始化Fragment
     */
    private void initFragment(){
        mCreateFragment = CreateFragment.getInstance();
        mScrawlFragment = ScrawlFragment.getInstance();
        mFindFragment = FindFragment.getInstance();
        mCreateFragment.getMainActivity(this);
        mScrawlFragment.getMainActivityShow(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_activity_show, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_gallery) {
            Log.e(TAG, "onNavigationItemSelected: 点击了作品树");
            Intent productTree = new Intent(MainActivityShow.this, ProductionTree.class);
            productTree.putExtra("where", "Main");
            startActivity(productTree);
        } else if (id == R.id.nav_my_friends) {
            Toast.makeText(MainActivityShow.this, "该功能为开放", Toast.LENGTH_LONG).show();
        } else if (id == R.id.nav_setting) {
            Intent intent1 = new Intent(MainActivityShow.this, Setting.class);
            startActivity(intent1);
        } else if (id == R.id.nav_share) {
            Toast.makeText(MainActivityShow.this, "该功能为开放", Toast.LENGTH_LONG).show();

        } else if (id == R.id.nav_logout) {
            BmobUser.logOut();   //清除缓存用户对象
            Intent intentToLogOut = new Intent(MainActivityShow.this, LogInOrRegisterActivity.class);
            startActivity(intentToLogOut);
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void replaceFragment(Fragment fragment){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fl_show, fragment);
        fragmentTransaction.commit();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.takePhoto:
                article="我猜你画的是";
                Log.e(TAG, "点击了拍照");
                takePhoto();

                break;
            case R.id.choosePhoto:
                Log.e(TAG, "点击了从相册中选择");
//                从相册中选择
                if(ContextCompat.checkSelfPermission(MainActivityShow.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED){
                    ActivityCompat.requestPermissions(MainActivityShow.this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},1);
                } else {
                    openAlbum();
                }
                break;

            case R.id.iv_whatYouPaint:
                if(resultCognition.length() != 0){
                    // 如果图片识别成功，点击图像弹出功能选择弹窗
                    Log.e(TAG, "点击了图片，要进行功能选择 ");
                    addChoiseEvent();
                }
                break;
        }
        if(requestFromScrawl){
            if(dialogForScrawl != null){
                dialogForScrawl.dismiss();
            }
        } else{
            if(dialog != null){
                dialog.dismiss();
            }
        }
    }

    /**
     * 调用系统相机进行拍照
     */
    private void takePhoto() {
        String status = Environment.getExternalStorageState();
        if(status.equals(Environment.MEDIA_MOUNTED)){
            //创建FIle对象，用于保存拍照后的图片
            File outputImage = new File(getExternalCacheDir(),
                    "output_image.jpg");
            try {
                //如果这张图片存在就将其删除
                if(outputImage.exists()){
                    outputImage.delete();
                }
                outputImage.createNewFile();
            }catch(Exception e){
                e.printStackTrace();
            }
            if(Build.VERSION.SDK_INT >= 24){
                //获取图片路径 这里的第二个参数要与Manifest.xml文件中的FileProvider中的authorities 一致
                imageUri = FileProvider.getUriForFile(MainActivityShow.this,
                        "com.example.zhuhaipeng.babypainter.MainFunctionModules.provider",
                        outputImage);
            }else{
                imageUri = Uri.fromFile(outputImage);
            }
            if(requestFromScrawl){
                //启动相机程序
                Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                intent.putExtra(MediaStore.EXTRA_OUTPUT,imageUri);
                //运行app时确认已经授权打开相机
                startActivityForResult(intent,TAKE_PHOTOForScrawl);
            } else{
                uploadPicture = imageUri.toString();
                Log.e(TAG, "imageUri:" + uploadPicture);
                //启动相机程序
                Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
//                String f = System.currentTimeMillis()+".jpg";
//                Uri fileUri = Uri.fromFile(new File(Environment.getExternalStoragePublicDirectory("").getPath()+f));
                intent.putExtra(MediaStore.EXTRA_OUTPUT,imageUri); //指定图片存放位置，指定后，在onActivityResult里得到的Data将为null
                //运行app时确认已经授权打开相机
                startActivityForResult(intent,TAKE_PHOTO);
            }
        }else{
            Log.e(TAG, "takePhoto:没有储存卡");
        }

    }

    /**
     * 打开相册
     */
    private void openAlbum(){
        if(requestFromScrawl){
            Intent intent = new Intent("android.intent.action.GET_CONTENT");
            intent.setType("image/*");
            startActivityForResult(intent,CHOOSE_PHOTOForScrawl);
        }else{
            Intent intent = new Intent("android.intent.action.GET_CONTENT");
            intent.setType("image/*");
            startActivityForResult(intent,CHOOSE_PHOTO);
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        Log.e(TAG, "onActivityResult:回调请求码 "+requestCode);
        UtilForPicture utilForPicture = UtilForPicture.getInstance();
        switch (requestCode) {
            case TAKE_PHOTO: // 选择打开相机照相
                if(resultCode == RESULT_OK){
                    try{
                        Bitmap bitmap = utilForPicture.comp(BitmapFactory.decodeStream(getContentResolver().openInputStream(imageUri)));
                        base64String = utilForPicture.getBitmapStrBase64(bitmap);
                        utilForPicture.requestConnect(new MainActivityShow(), access_token, base64String);
                    }catch (FileNotFoundException e){
                        Log.e(TAG, "onActivityResult: 没获取到图片" );
                        Toast.makeText(MainActivityShow.this,"对不起，您的机型暂未完全适配，未获取到图片！",Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                }
                break;
            case CHOOSE_PHOTO: // 从相册中选择
                if(resultCode == RESULT_OK){
                    //判断手机系统版本号
                    if(Build.VERSION.SDK_INT >= 19){
                        //4.4及以上手机系统使用这个方法处理图片
                        uploadPicture = utilForPicture.handleImageOnKitKat(MainActivityShow.this, access_token, data);
                    } else {
                        //4.4以下手机系统使用这个方法处理图片
                        uploadPicture = utilForPicture.handleImageBeforKitKat(MainActivityShow.this, access_token, data);
                    }
                }
                break;
            case TAKE_PHOTOForScrawl: // 选择打开相机照相
                if (resultCode == RESULT_OK) {
                    try{
                        Random random = new Random();
                        Integer i = random.nextInt(10) + 1;
                        String pos = i.toString();
                        Glide.with(this)
                                .load(mScrawlFragment.getPicturToScrawl().get(pos))
                                .into(mScrawlFragment.showPicture);
                        MainActivityShow.kqwSpeechCompound.speaking("点击图片还可以与小新说话哦");
                        ScrawlFragment.canTalk = true;
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
                break;

            case CHOOSE_PHOTOForScrawl: // 从相册中选择
                Log.e(TAG, "onActivityResult:...........");
                if (resultCode == RESULT_OK) {
                    Log.e(TAG, "onActivityResult:~~~~~~~~~~~~~~~~~");
                    //判断手机系统版本号
                    if (Build.VERSION.SDK_INT >= 19) {
                        Log.e(TAG, "onActivityResult:--->handleImageOnKitKat");
                        //4.4及以上手机系统使用这个方法处理图片
                        mScrawlFragment.handleImageOnKitKat(data);
                    } else {
                        //4.4以下手机系统使用这个方法处理图片
                        Log.e(TAG, "onActivityResult:--->handleImageBeforKitKat");
                        mScrawlFragment.handleImageBeforKitKat(data);
                    }
                    ScrawlFragment.canTalk = true;
                }
                Log.e(TAG, "onActivityResult:XXXXXXXXXXXXXXXXXX");
                break;
            default:
                break;
        }
    }

    /**
     * 利用回调，将图片识别结果回传
     * @param result 服务器返回的图像识别结果
     */
    public void getRecognitionResult( String result){
            String article = "我猜你画的是";
            if(null != result){
            Log.e(TAG, "getRecognitionResult:"+result );
            setPictureRecognitionResult(result);

        }
    }

    /**
     * 显示图片识别的结果
     */
    private void setPictureRecognitionResult(final String result) {
        // 用Activity对象的runOnUiThread方法更新
        //在子线程中通过runOnUiThread()方法更新UI
        new Thread(){
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mCreateFragment.tv_hint.setText(resultCognition);
                        Random random = new Random();
                        int fenshu = 60;
                        switch (result) {
                            case "apple":
                                updataForUser(uploadPicture, result);
                                queryForUser(result);
                                fenshu = random.nextInt(25) + 75;
                                index = 1;
                                resultCognition.append("苹果" + "\t\t\t\t" + "apple");
                                kqwSpeechCompound.speaking("你画的是苹果，画的真棒，偷偷告诉你哦," +
                                        "苹果在英语里叫做apple，再告诉你，双击图片有惊喜哦！");
                                mCreateFragment.iv_figure.setVisibility(View.VISIBLE);
                                Glide.with(mCreateFragment).load(R.mipmap.apple).into(mCreateFragment.whatYouPaint);
                                Glide.with(mCreateFragment).load(R.drawable.figure).into(mCreateFragment.iv_figure);
                                break;
                            case "dog":
                                updataForUser(uploadPicture, result);
                                queryForUser(result);
                                fenshu = random.nextInt(25) + 75;
                                index = 2;
                                resultCognition.append("狗" + "\t\t\t\t" + "dog");
                                kqwSpeechCompound.speaking("你画的是小狗,太厉害了，画的真像，偷偷告诉你哦," +
                                        "小狗在英语里叫做dog，再告诉你，双击图片有惊喜哦！");
                                mCreateFragment.iv_figure.setVisibility(View.VISIBLE);
                                Glide.with(mCreateFragment).load(R.mipmap.dog).into(mCreateFragment.whatYouPaint);
                                Glide.with(mCreateFragment).load(R.drawable.figure).into(mCreateFragment.iv_figure);
                                break;
                            case "turtle":
                                updataForUser(uploadPicture, result);
                                queryForUser(result);
                                fenshu = random.nextInt(25) + 75;
                                index = 2;
                                resultCognition.append("乌龟" + "\t\t\t\t" + "turtle");
                                kqwSpeechCompound.speaking("你画的是乌龟,画的不错，偷偷告诉你哦,乌龟在英语里叫做turtle，再告诉你，双击图片有惊喜哦！");
                                mCreateFragment.iv_figure.setVisibility(View.VISIBLE);
                                Glide.with(mCreateFragment).load(R.mipmap.turtle).into(mCreateFragment.whatYouPaint);
                                Glide.with(mCreateFragment).load(R.drawable.figure).into(mCreateFragment.iv_figure);
                                break;
                            case "rabbit":
                                updataForUser(uploadPicture, result);
                                queryForUser(result);
                                fenshu = random.nextInt(25) + 75;
                                index = 2;
                                resultCognition.append("兔子" + "\t\t\t\t" + "rabbit");
                                kqwSpeechCompound.speaking("你画的是小兔子,画的真可爱，偷偷告诉你哦,兔子在英语里叫做rabbit，再告诉你，双击图片有惊喜哦！");
                                mCreateFragment.iv_figure.setVisibility(View.VISIBLE);
                                Glide.with(mCreateFragment).load(R.mipmap.r).into(mCreateFragment.whatYouPaint);
                                Glide.with(mCreateFragment).load(R.drawable.figure).into(mCreateFragment.iv_figure);
                                break;
                            case "chicken":
                                updataForUser(uploadPicture, result);
                                queryForUser(result);
                                fenshu = random.nextInt(25) + 75;
                                index = 2;
                                resultCognition.append("小鸡" + "\t\t\t\t" + "chicken");
                                kqwSpeechCompound.speaking("你画的是小鸡,画的真可爱，偷偷告诉你哦,小鸡在英语里叫做chicken，再告诉你，双击图片有惊喜哦！");
                                mCreateFragment.iv_figure.setVisibility(View.VISIBLE);
                                Glide.with(mCreateFragment).load(R.mipmap.chicken).into(mCreateFragment.whatYouPaint);
                                Glide.with(mCreateFragment).load(R.drawable.figure).into(mCreateFragment.iv_figure);
                                break;
                            case "huocairen":
                                updataForUser(uploadPicture, result);
                                queryForUser(result);
                                fenshu = random.nextInt(25) + 75;
                                index = 2;
                                resultCognition.append("火柴人" + "\t\t\t\t" + "huocairen");
                                kqwSpeechCompound.speaking("你画的是火柴人,画的不错喔，偷偷告诉你哦,人在英语里叫做person，再告诉你，双击图片有惊喜哦！");
                                mCreateFragment.iv_figure.setVisibility(View.VISIBLE);
                                Glide.with(mCreateFragment).load(R.mipmap.huocairen).into(mCreateFragment.whatYouPaint);
                                Glide.with(mCreateFragment).load(R.drawable.figure).into(mCreateFragment.iv_figure);
                                break;
                            case "[default]":
                                resultCognition = new StringBuffer("虽然没猜出你画的是啥......." + "\n" + "但是看起来挺不错的");
                                kqwSpeechCompound.speaking("虽然没猜出你画的是啥，但是看起来挺不错的,你要不再试试啊");
                                mCreateFragment.iv_figure.setVisibility(View.INVISIBLE);
                                break;
                            default:
                                break;
                        }
                        mCreateFragment.tv_hint.setText(resultCognition);
                        if(fenshu > 70){
                            mCreateFragment.tv_grade.setText(fenshu + "");
                            mCreateFragment.tv_fen.setText("分");
                        }else{
                            mCreateFragment.tv_fen.setText("");
                        }
                        resultCognition = new StringBuffer(" ");
                    }

                });
            }
        }.start();
    }

    /**
     * 用于打开选择的dialog
     */
    public void addChoiseEvent()
    {
        final List<String> values = new ArrayList<String>();
        values.add("视频");
        values.add("儿歌");
        values.add("梦幻世界");
        final List<String> keys = new ArrayList<String>();
        keys.add("1");
        keys.add("2");
        keys.add("3");
        mCreateFragment.whatYouPaint.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mCreateFragment.Choise(values,keys,MainActivityShow.this ,new DoubleAction(){
                    @Override
                    public void actionDouble(String _value, String _key) {
                        // TODO Auto-generated method stub
                    }
                },"请选择功能");
            }
        });
    }
    /**
     * 将识别成功的图片更新到用户画过的图片中
     */
    private void updataForUser(String uri, final String name){
        //上传头像
        final String picPath = uri.toString();
        final BmobFile bmobFile = new BmobFile(new File(picPath));
        bmobFile.uploadblock(new UploadFileListener() {
            @Override
            public void done(BmobException e) {
                if(e==null){
                    //bmobFile.getFileUrl()--返回的上传文件的完整地址
                    MyUser myUser1 = BmobUser.getCurrentUser(MyUser.class);
                    myUser1.addUnique("Painted", bmobFile.getFileUrl());
                    myUser1.setPaintedPicture(name);
                    myUser1.update(new UpdateListener() {
                        @Override
                        public void done(BmobException e) {
                            if(e==null){
                                Log.e(TAG, "将图片成功添加到作品集");
                            }else{
                                Log.e(TAG, "添加失败");
                            }
                        }
                    });
                }else{
                    Log.e(TAG, "上传文件失败");
                }

            }
            @Override
            public void onProgress(Integer value) {
                // 返回的上传进度（百分比）
            }
        });
    }
    /**
     * 查询哪个用户画过同样的图片
     * @param s 图片的类型
     */
    private void queryForUser(final String s){
        final MyUser user = BmobUser.getCurrentUser(MyUser.class);
        BmobQuery<MyUser> query = new BmobQuery<MyUser>();
        query.addWhereEqualTo("paintedPicture", s);
        query.findObjects(new FindListener<MyUser>() {
            @Override
            public void done(List<MyUser> object,BmobException e) {
                if(e==null){
                    for (MyUser myUser : object){
                        if(!myUser.getObjectId().equals(user.getObjectId())){
                            String painted;
                            painted = myUser.getPaintedPicture();
                            if(painted.equals(s)){
                                Toast.makeText(MainActivityShow.this,"用户："+myUser.getUsername()+"  最近也画过同样的画哦！",Toast.LENGTH_LONG).show();
                                Log.e(TAG, "用户："+myUser.getUsername()+"也画过同样的画哦！");
                                break;
                            }
                        }
                    }
                }else{
                    Log.e(TAG, "query:---->"+e.getMessage());
                }
            }
        });
    }

}
