package com.example.zhuhaipeng.babypainter.bean;

/**
 * Auto-generated: 2019-04-19 23:25:13
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class InputImage {

    private String url;
    public void setUrl(String url) {
        this.url = url;
    }
    public String getUrl() {
        return url;
    }

}