package com.example.zhuhaipeng.babypainter.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.zhuhaipeng.babypainter.R;
import com.example.zhuhaipeng.babypainter.VideoTeach.VedioTeach;
import com.example.zhuhaipeng.babypainter.VideoTeach.VideoForPaint;
import com.example.zhuhaipeng.babypainter.VideoTeach.VideoTeaching;


import java.util.List;

public class VideoForPaintAdapter extends RecyclerView.Adapter<VideoForPaintAdapter.ViewHolder>
        implements View.OnClickListener {
    private Context context;
    private List<VideoForPaint> list;
    private static final String TAG = "VideoForPaintAdapter";

    static class ViewHolder extends RecyclerView.ViewHolder{
        CardView cardView;
        LinearLayout mLinearLayout;
        ImageView imageView;
        TextView textView;
        public ViewHolder(View view){
            super(view);
            cardView = (CardView) view;
            mLinearLayout = (LinearLayout) view.findViewById(R.id.item_for_video_teaching);
            imageView = (ImageView) view.findViewById(R.id.image_item);
            textView = (TextView) view.findViewById(R.id.name_item);
        }
    }
    public VideoForPaintAdapter(List<VideoForPaint> list){
        this.list = list;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        if(context == null){
            context = viewGroup.getContext();
        }
        View view = LayoutInflater.from(context).inflate(R.layout.recyclerview_item_with_video,viewGroup,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        VideoForPaint videoForPaint = list.get(i);
        viewHolder.textView.setText(videoForPaint.getName());
        Glide.with(context).load(videoForPaint.getId()).into(viewHolder.imageView);
        viewHolder.mLinearLayout.setOnClickListener(this);

    }

    @Override
    public int getItemCount() {
        return list.size();
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.item_for_video_teaching:
                Intent intent = new Intent(v.getContext(), VedioTeach.class);
                v.getContext().startActivity(intent);
                Log.e(TAG, "选择了一部教程");
                break;
            default:
                break;
        }
    }

}
