package com.example.zhuhaipeng.babypainter.AllBmobClass;

import java.util.List;

import cn.bmob.v3.BmobUser;

public class MyUser extends BmobUser {
    private String sex;
    private String Image;
    private String age;
    private String UserImage;
    private List<String> Painted;
    private String grade;
    private String paintedPicture;

    public String getPaintedPicture() {
        return paintedPicture;
    }

    public void setPaintedPicture(String paintedPicture) {
        this.paintedPicture = paintedPicture;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public List<String> getPainted() {
        return Painted;
    }

    public void setPainted(List<String> Painted) {
        this.Painted = Painted;
    }

    public String getUserImage() {
        return UserImage;
    }

    public void setUserImage(String userImage) {
        UserImage = userImage;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getImage() {
        return this.Image;
    }

    public void setImage(String image) {
        this.Image = image;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }
}
