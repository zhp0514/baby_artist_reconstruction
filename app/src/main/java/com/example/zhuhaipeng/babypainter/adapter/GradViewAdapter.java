package com.example.zhuhaipeng.babypainter.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.zhuhaipeng.babypainter.MainFunctionModules.MainActivityShow;
import com.example.zhuhaipeng.babypainter.R;

import java.util.List;

public class GradViewAdapter extends BaseAdapter {
    private Context context;
    private  List<String > url;
    private static final String TAG = "GradViewAdapter";
    private LayoutInflater inflater;

    public GradViewAdapter(Context context,List<String> url) {
        super();
        this.context = context;
        this.url = url;
        inflater  = LayoutInflater.from(context);

    }

    @Override
    public int getCount() {
        return url.size() ;
    }

    @Override
    public Object getItem(int position) {
        return url.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
//        Log.e(TAG, "这是第"+position +"位置");
        ViewHolder holder = new ViewHolder();
        if(view == null){
            view = inflater.inflate(R.layout.layout_grid_item,null);
            holder = new ViewHolder();
            holder.imageView = (ImageView) view.findViewById(R.id.ItemImage);
            view.setTag(holder);

        }else{
            holder = (ViewHolder) view.getTag();
        }
        ViewGroup.LayoutParams para = holder.imageView.getLayoutParams();
        para.width = (MainActivityShow.mScreenWidth)/ 2;//一屏显示3列
        para.height = (MainActivityShow.mScreenWidth )/ 3;
        holder.imageView.setLayoutParams(para);
        if(context != null){
            Glide.with(context).load(url.get(position)).into(holder.imageView);
        }
        return view;
    }


}

class ViewHolder{
  public ImageView imageView;

}
