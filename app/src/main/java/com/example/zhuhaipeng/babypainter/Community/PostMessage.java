package com.example.zhuhaipeng.babypainter.Community;

import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.zhuhaipeng.babypainter.AllBmobClass.MyUser;
import com.example.zhuhaipeng.babypainter.AllBmobClass.Post;
import com.example.zhuhaipeng.babypainter.R;
import com.example.zhuhaipeng.babypainter.adapter.CommentExpandAdapter;
import com.example.zhuhaipeng.babypainter.adapter.GradViewAdapter;
import com.example.zhuhaipeng.babypainter.bean.Comment;
import com.example.zhuhaipeng.babypainter.bean.ReplyDetailBean;
import com.example.zhuhaipeng.babypainter.view.CommentExpandableListView;

import java.util.ArrayList;
import java.util.List;

import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.BmobUser;
import cn.bmob.v3.datatype.BmobPointer;
import cn.bmob.v3.datatype.BmobRelation;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;
import cn.bmob.v3.listener.QueryListener;
import cn.bmob.v3.listener.SaveListener;
import cn.bmob.v3.listener.UpdateListener;

public class PostMessage extends AppCompatActivity implements  View.OnClickListener{
    private TextView article;
    private static final String TAG = "PostMessage";
    private TextView name;
    private CommentExpandAdapter adapter;
    private ImageView userImage;
    private GridView gridView;
    private CommentExpandableListView expandableListView;

    private List<Comment> commentsList;
    private BottomSheetDialog dialog;
    private TextView bt_comment;
    private  Post post;

    private ActionBar actionBar; //头部栏
    private ImageView circleImageView; //头部的返回箭头

    private TextView browseCount;
    private ImageView isLiked;
    private String s;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_message);
        post = (Post) getIntent().getSerializableExtra("Post");
        s = (String) getIntent().getStringExtra("flag");
        initView();
        if(post.getPicture() != null){
            GradViewAdapter gradViewAdapter = new GradViewAdapter(PostMessage.this,post.getPicture());
            gridView.setAdapter(gradViewAdapter);
        }else{

        }
        // 设置头部bar
        actionBar = getSupportActionBar();
        if(actionBar != null){
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setCustomView(R.layout.community_bar);
            circleImageView = findViewById(R.id.backToFind);
            //            choosePlus = findViewById(R.id.startRecognition1);
            circleImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
        }
        browseCount.setText(String.valueOf(post.getBrowseNumber()));
        if(post.getBrowseNumber() != 0){
            Glide.with(this).load(R.drawable.red_heart).into(isLiked);
        }else{
            Glide.with(this).load(R.drawable.heart).into(isLiked);
        }
        article.setText(post.getContent());
        name.setText(post.getAuthor().getUsername());
        Glide.with(PostMessage.this).load(post.getAuthor().getImage()).into(userImage);
        if(s.equals("2")){
            showCommentDialog();
        }
    }

    private void initView() {
        expandableListView = (CommentExpandableListView) findViewById(R.id.detail_page_lv_comment);
        bt_comment = (TextView) findViewById(R.id.detail_page_do_comment);
        bt_comment.setOnClickListener(this);
        article = (TextView) findViewById(R.id.articleforMessage);
        name = (TextView) findViewById(R.id.user_name);
        userImage = (ImageView) findViewById(R.id.circleImageViewforPost);
        gridView = (GridView) findViewById(R.id.Clickgradviewforpost2);
        article.setOnClickListener(this);
        name.setOnClickListener(this);
        userImage.setOnClickListener(this);
        isLiked = (ImageView) findViewById(R.id.dian_zan_for_post);
        isLiked.setOnClickListener(this);
        browseCount = (TextView) findViewById(R.id.liu_lan_ci_shu_for_post);
        generateTestData(commentsList); // 获取数据

    }
    /**
     * 获取评论数据
     */
    private void generateTestData(List<Comment> comments){
//        commentBean = gson.fromJson(testJson, CommentBean.class);
        // 查询评论数据
        comments= new ArrayList<>();
        final List<Comment> commentList = comments;
        BmobQuery<Comment> query = new BmobQuery<>();
        Post post = this.post;
        query.addWhereEqualTo("post",new BmobPointer(post));
        query.include("user");
        query.findObjects(new FindListener<Comment>() {
            @Override
            public void done(List<Comment> list, BmobException e) {
                if(e==null){
                    Log.e(TAG, "评论数据条数"+list.size());
                    commentList.addAll(list);
                    initExpandableListView(commentList);
                }
            }
        });
    }
    private void getData(List<Comment> comments){
        //        commentBean = gson.fromJson(testJson, CommentBean.class);

    }
    /**
     * 初始化评论和回复列表
     */
    private void initExpandableListView(final List<Comment> commentList){
        //ExpandableListView在默认情况下会为我们自带分组的icon（▶）
        expandableListView.setGroupIndicator(null);
        //默认展开所有回复
        adapter = new CommentExpandAdapter(this, commentList);
        expandableListView.setAdapter(adapter);
        for(int i = 0; i<commentList.size(); i++){
            expandableListView.expandGroup(i);
        }
        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView expandableListView, View view, int groupPosition, long l) {
                boolean isExpanded = expandableListView.isGroupExpanded(groupPosition);
                Log.e(TAG, "onGroupClick: 当前的评论的作者>>>"+commentList.get(groupPosition).getUser().getUsername());
                // 展开group
//                if(isExpanded){
//                    expandableListView.collapseGroup(groupPosition);
//                }else {
//                    expandableListView.expandGroup(groupPosition, true);
//                }
                showReplyDialog(groupPosition,commentList);
                return true;
            }
        });

        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView expandableListView, View view, int groupPosition, int childPosition, long l) {
                Toast.makeText(PostMessage.this,"点击了回复",Toast.LENGTH_SHORT).show();
                return false;
            }
        });

        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                //toast("展开第"+groupPosition+"个分组");

            }
        });

    }
    /**
     * by moos on 2018/04/20
     * func:弹出回复框
     */
    private void showReplyDialog(final int position,final List<Comment> comments){
        dialog = new BottomSheetDialog(this);
        View commentView = LayoutInflater.from(this).inflate(R.layout.comment_dialog_layout,null);
        final EditText commentText = (EditText) commentView.findViewById(R.id.dialog_comment_et);
        final Button bt_comment = (Button) commentView.findViewById(R.id.dialog_comment_bt);
        Log.e(TAG, "showReplyDialog: "+comments.size() );
        commentText.setHint("回复 " + comments.get(position).getUser().getUsername() + " 的评论:");
        dialog.setContentView(commentView);
        bt_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String replyContent = commentText.getText().toString().trim();
                if(!TextUtils.isEmpty(replyContent)){
                    dialog.dismiss();
//                    // 回复评论
                    final MyUser user = BmobUser.getCurrentUser(MyUser.class);
                    //先查询评论数据
                    BmobQuery<Comment> query = new BmobQuery<>();
                    query.getObject(comments.get(position).getObjectId(), new QueryListener<Comment>() {
                        @Override
                        public void done(Comment comment, BmobException e) {
                            // 查询完数据后，更新数据
                            if(e==null){
                                Log.e(TAG, "查询回复数据成功");
                                final ReplyDetailBean detailBean = new ReplyDetailBean(user.getUsername(),replyContent);
                                detailBean.setNickName(user.getUsername());
                                detailBean.setContent(replyContent);
                                List<ReplyDetailBean> replayList = new ArrayList<>();
                                replayList.addAll(comment.getReplyList());
                                replayList.add(detailBean);
                                Comment comment1 = new Comment();
                                comment1.setObjectId(comments.get(position).getObjectId());
                                comment1.increment("replyTotal");
                                comment1.setReplyList(replayList);
                                comment1.update(new UpdateListener() {
                                    @Override
                                    public void done(BmobException e) {
                                        if(e==null){
                                            Log.e(TAG, "==更新评论的回复条数成功==");
                                            adapter.addTheReplyData(detailBean, position);
                                            expandableListView.expandGroup(position);
                                            Toast.makeText(PostMessage.this,"回复成功",Toast.LENGTH_SHORT).show();
                                        }else{
                                            Log.e(TAG, "--更新评论回复条数失败--"+e.getMessage());
                                        }
                                    }
                                });
                            }else{
                                Log.e(TAG, "查询回复数据失败");
                            }
                        }
                    });

//                    adapter.addTheReplyData(detailBean, position);
//                    expandableListView.expandGroup(position);
                }else {
                    Toast.makeText(PostMessage.this,"回复内容不能为空",Toast.LENGTH_SHORT).show();
                }
            }
        });
        commentText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(!TextUtils.isEmpty(charSequence) && charSequence.length()>2){
                    bt_comment.setBackgroundColor(Color.parseColor("#FFB568"));
                }else {
                    bt_comment.setBackgroundColor(Color.parseColor("#D8D8D8"));
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        dialog.show();
    }
    /**
     * by moos on 2018/04/20
     * func:弹出评论框
     */
    private void showCommentDialog(){
        dialog = new BottomSheetDialog(this);
        View commentView = LayoutInflater.from(this).inflate(R.layout.comment_dialog_layout,null);
        final EditText commentText = (EditText) commentView.findViewById(R.id.dialog_comment_et);
        final Button bt_comment = (Button) commentView.findViewById(R.id.dialog_comment_bt);
        dialog.setContentView(commentView);
        /**
         * 解决bsd显示不全的情况
         */
        View parent = (View) commentView.getParent();
        BottomSheetBehavior behavior = BottomSheetBehavior.from(parent);
        commentView.measure(0,0);
        behavior.setPeekHeight(commentView.getMeasuredHeight());
        final Post post = this.post;
        bt_comment.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                String commentContent = commentText.getText().toString().trim();
                if(!TextUtils.isEmpty(commentContent)){

                    //commentOnWork(commentContent);
                    dialog.dismiss();
                    // 更新评论列表
                    MyUser user = BmobUser.getCurrentUser(MyUser.class);
                    final Comment comment = new Comment();
                    comment.setContent(commentContent);
                    comment.setPost(post);
                    comment.setUser(user);
                    comment.save(new SaveListener<String>() {
                        @Override
                        public void done(String objectId, BmobException e) {
                            if(e==null){
                                Log.i("bmob","评论发表成功");
                            }else{
                                Log.i("bmob","失败："+e.getMessage());
                            }
                        }

                    });

//                    Comment detailBean = new Comment("小明", commentContent,"刚刚");
                    adapter.addTheCommentData(comment);
                    Toast.makeText(PostMessage.this,"评论成功",Toast.LENGTH_SHORT).show();

                }else {
                    Toast.makeText(PostMessage.this,"评论内容不能为空",Toast.LENGTH_SHORT).show();
                }
            }
        });
        commentText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(!TextUtils.isEmpty(charSequence) && charSequence.length()>2){
                    bt_comment.setBackgroundColor(Color.parseColor("#FFB568"));
                }else {
                    bt_comment.setBackgroundColor(Color.parseColor("#D8D8D8"));
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        dialog.show();
    }


    // 点击评论
    @Override
    public void onClick(View v) {
       if(v.getId() == R.id.detail_page_do_comment){
            showCommentDialog();
       }
       if(v.getId() == R.id.dian_zan_for_post){
            query(isLiked);
       }
    }
    // 查询
    private void query(final ImageView view){
        // 获得当前登陆的用户
        final MyUser myUser = BmobUser.getCurrentUser(MyUser.class);
        BmobQuery<MyUser> bmobQuery = new BmobQuery<MyUser>();
        //likes是Post表中的字段，用来存储所有喜欢该帖子的用户
        bmobQuery.addWhereRelatedTo("likes",new BmobPointer(post));
        // 查询
        bmobQuery.findObjects(new FindListener<MyUser>() {
            @Override
            public void done(List<MyUser> list, BmobException e) {
                if(e==null){
                    Log.e(TAG, " 喜欢该帖子的人数： "+list.size() );
                    // 判断当前喜欢该帖子的用户人数，如果大于零，用户点赞后进行相关判断
                    if(list.size() > 0){
                        int count = 0;
                        for(MyUser user : list){
                            // 如果已经点过赞，再次点击后赞会取消，浏览次数减一
                            if(user.getObjectId().equals(myUser.getObjectId())){
                                // 设置点赞图片的状态
                                Resources resources = PostMessage.this.getResources();
                                Drawable drawable = resources.getDrawable(R.drawable.heart);
                                isLiked.setImageDrawable(drawable);

                                MyUser myUser1 = BmobUser.getCurrentUser(MyUser.class);
                                String id = post.getObjectId();
                                Post post1 = new Post();
                                post1.setObjectId(id);
                                BmobRelation relation = new BmobRelation();
                                relation.remove(myUser1);
                                post1.setLikes(relation);
                                post1.increment("browseNumber",-1);
                                post1.update(new UpdateListener() {
                                    @Override
                                    public void done(BmobException e) {
                                        if(e==null){
                                            Log.e("bmob","关联关系删除成功");
                                            queryAgain(post);
                                        }else{
                                            Log.e("bmob","失败："+e.getMessage());
                                        }
                                    }
                                });

                                break;
                            }else{
                                ++count;
                            }
                        }
                        //                        // 说明当前用户没有给该帖子点过赞
                        if(count >= list.size()){
                            // 设置点赞图片的状态
                            Resources resources = PostMessage.this.getResources();
                            Drawable drawable = resources.getDrawable(R.drawable.red_heart);
                            isLiked.setImageDrawable(drawable);


                            String id = post.getObjectId();
                            Post post1 = new Post();
                            //将当前用户添加到Post表中的likes字段值中，表明当前用户喜欢该帖子
                            BmobRelation relation = new BmobRelation();
                            //将当前用户添加到多对多关联中
                            relation.add(myUser);
                            //多对多关联指向`post`的`likes`字段
                            post1.setLikes(relation);
                            post1.increment("browseNumber");
                            post1.update(id,new UpdateListener() {
                                @Override
                                public void done(BmobException e) {
                                    if(e==null){
                                        Log.e(TAG, "done: 更新次数成功");
                                        queryAgain(post);
                                    }else{
                                        Log.e(TAG, "done: 更新次数失败" );
                                    }
                                }
                            });
                            count = 0;
                        }
                    } else {  //当前帖子无用户点赞，现用户点赞的情况
                        // 设置点赞图片的状态
                        Resources resources = PostMessage.this.getResources();
                        Drawable drawable = resources.getDrawable(R.drawable.red_heart);
                        isLiked.setImageDrawable(drawable);

                        String id = post.getObjectId();
                        final Post post1 = new Post();
                        //将当前用户添加到Post表中的likes字段值中，表明当前用户喜欢该帖子
                        BmobRelation relation = new BmobRelation();
                        //将当前用户添加到多对多关联中
                        relation.add(myUser);
                        //多对多关联指向`post`的`likes`字段
                        post1.setLikes(relation);
                        post1.increment("browseNumber");
                        post1.update(id,new UpdateListener() {
                            @Override
                            public void done(BmobException e) {
                                if(e==null){
                                    Log.e(TAG, "done: 无点赞时更新次数成功");
                                    queryAgain(post);
                                }else{
                                    Log.e(TAG, "done: 无点赞时更新次数失败" );
                                }
                            }
                        });
                    }

                }
            }
        });

    }
    private void queryAgain( Post post){
        Log.e(TAG, "浏览次数 "+post.getBrowseNumber());
        BmobQuery<Post> query = new BmobQuery<>();
        query.getObject(post.getObjectId(), new QueryListener<Post>() {
            @Override
            public void done(Post object, BmobException e) {
                if(e==null){
                    browseCount.setText(String.valueOf(object.getBrowseNumber()));
                }else{
                    Log.i("bmob","失败："+e.getMessage()+","+e.getErrorCode());
                }
            }

        });
    }
}
