package com.example.zhuhaipeng.babypainter.VideoTeach;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.VideoView;

import com.example.zhuhaipeng.babypainter.R;

public class AnimalVedio extends AppCompatActivity  implements View.OnTouchListener{

    private VideoView videoView;
    private ImageView back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animal_vedio);


        back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AnimalVedio.this,VideoTeaching.class);
                startActivity(intent);
                finish();
            }
        });
        videoView = (VideoView) findViewById(R.id.videoViewForAnimal);
        videoView.setVideoPath("http://bmob-cdn-19476.b0.upaiyun.com/2018/11/15/7d969f46400c5474805628859fe6356c.mp4");
        if(!videoView.isPlaying()){
            videoView.start();
        }
    }
    @Override
    public void onBackPressed() {
        //此处写退向后台的处理
        this.finish();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);//设置videoView全屏播放
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);//设置videoView横屏播放
        WindowManager wm = (WindowManager) this.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics dm = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels;         // 屏幕宽度（像素）
        int height = dm.heightPixels;
        float density = dm.density;         // 屏幕密度（0.75 / 1.0 / 1.5）
        int densityDpi = dm.densityDpi;     // 屏幕密度dpi（120 / 160 / 240）
        // 屏幕宽度算法:屏幕宽度（像素）/屏幕密度
        int screenWidth = (int) (width / density);  // 屏幕宽度(dp)
        int screenHeight = (int) (height / density);// 屏幕高度(dp)
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) videoView.getLayoutParams(); // 取控当前的布局参数
        layoutParams.height = screenHeight;//设置 当控件的高强
        layoutParams.width = screenWidth;
        videoView.setLayoutParams(layoutParams); // 使设置好的布局参数应用到控件
        return super.onTouchEvent(event);
    }
}
