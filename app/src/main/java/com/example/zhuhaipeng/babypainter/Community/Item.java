package com.example.zhuhaipeng.babypainter.Community;

import java.util.ArrayList;
import java.util.HashMap;

public class Item {
    private String title;
    private String article;

    public String getArticle() {
        return article;
    }

    public void setArticle(String article) {
        this.article = article;
    }

    private ArrayList<HashMap<String, Object>> img;

    public Item() {

    }

    public Item(String title, ArrayList<HashMap<String, Object>> img) {
        this.title = title;
        this.img = img;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<HashMap<String, Object>> getImg() {
        return img;
    }

    public void setImg(ArrayList<HashMap<String, Object>> img) {
        this.img = img;
    }
}
