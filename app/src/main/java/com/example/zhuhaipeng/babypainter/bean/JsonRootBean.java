package com.example.zhuhaipeng.babypainter.bean; /**
 * Copyright 2019 bejson.com
 */

import com.example.zhuhaipeng.babypainter.bean.Perception;

/**
 * Auto-generated: 2019-04-19 11:18:53
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
/**
 * Copyright 2019 bejson.com
 */

public class JsonRootBean {

    private int reqType;
    private Perception perception;
    private UserInfo userInfo;
    public void setReqType(int reqType) {
        this.reqType = reqType;
    }
    public int getReqType() {
        return reqType;
    }

    public void setPerception(Perception perception) {
        this.perception = perception;
    }
    public Perception getPerception() {
        return perception;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }
    public UserInfo getUserInfo() {
        return userInfo;
    }

}