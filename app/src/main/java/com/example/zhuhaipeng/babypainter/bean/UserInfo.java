package com.example.zhuhaipeng.babypainter.bean; /**
 * Copyright 2019 bejson.com
 */


/**
 * Auto-generated: 2019-04-19 11:18:53
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class UserInfo {

    private String apiKey;
    private String userId;
    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }
    public String getApiKey() {
        return apiKey;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
    public String getUserId() {
        return userId;
    }

}