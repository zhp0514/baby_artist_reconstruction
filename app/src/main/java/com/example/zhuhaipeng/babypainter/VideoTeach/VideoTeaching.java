package com.example.zhuhaipeng.babypainter.VideoTeach;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;


import com.example.zhuhaipeng.babypainter.R;
import com.example.zhuhaipeng.babypainter.adapter.VideoForPaintAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class VideoTeaching extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "VideoTeaching";
    private ImageView view;
    private ImageView view1;
    private VideoForPaint[] videoForPaints = {
            new VideoForPaint("dog", R.drawable.dog),
            new VideoForPaint("left",R.drawable.left_arrow),
            new VideoForPaint("right",R.drawable.right),
            new VideoForPaint("heart",R.drawable.heart),
    };
    private List<VideoForPaint> list = new ArrayList<>();
    private VideoForPaintAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_teaching);
        initData();
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.videoTeachingRecyclerView);
        GridLayoutManager layoutManager = new GridLayoutManager(this,2);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new VideoForPaintAdapter(list);
        recyclerView.setAdapter(adapter);
    }

    private void initData(){
        list.clear();
        for(int i = 0;i < 20;i++){
            Random random = new Random();
            int index = random.nextInt(videoForPaints.length);
            list.add(videoForPaints[index]);
        }
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.primary_tutorial:
                Log.e(TAG, "onClick: ");
                break;
            default:
                break;
        }
    }
}
